/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;
import sftp.lib.Console;
import sftp.lib.socket.Entity.FileInfo;
import sftp.lib.socket.Entity.TransferFileInfo;
import sftp.lib.socket.FileHandleType;

/**
 * @file FileTransferManageCenter.java
 * @author Wensheng Yan
 * @date Apr 6, 2014
 */
public class FileTransferManageCenter {
    private static Queue<String> waitlists = new ArrayDeque<String>();
    private static Queue<String> worklists = new ArrayDeque<String>();
    private static HashMap<String, TransferFileInfo> info = new HashMap<>();
    
    public static void add(FileInfo in, String FromDir, String ToDir, FileHandleType type){
        TransferFileInfo fileInfo = new TransferFileInfo(type, FromDir, ToDir, in.getFileName(), in.getFileSize());
        waitlists.offer(in.getFileName());
        info.put(in.getFileName(), fileInfo);
    }
    
    public static void addWork(String name){
        worklists.add(name);
    }
    
    public static void update(String filename, long reclen, double progress, long currentpkg){
        TransferFileInfo  fileInfo = info.get(filename);
        fileInfo.setLastRecSize(fileInfo.getLastRecSize() + reclen);
        fileInfo.setTotalRecSize(fileInfo.getTotalRecSize() + reclen);
        fileInfo.setProgress(progress);
        fileInfo.setCurrentPkg(currentpkg);
    }
    
    public static void updateMSG(String filename, String msg){
        TransferFileInfo  fileInfo = info.get(filename);
        if(fileInfo != null){
         fileInfo.setMessage(msg);
        }
    }
    
    public static void remove(String filename){
        waitlists.remove(filename);
        worklists.remove(filename);
        info.remove(filename);
    }
    
    public static String getFile(){
        return waitlists.peek();
    }
    
    public static String pollFile(){
        return waitlists.poll();
    }
    
    public static TransferFileInfo FileDetail(String name){
        return info.get(name);
    }
    
    public static int size(){
        return waitlists.size();
    }
    
}
