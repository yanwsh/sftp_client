/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.MediaView;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Client;
import sftp.client.FileTransferManageCenter;
import sftp.client.Utility;
import sftp.client.protocol.GetSessionKey;
import sftp.client.protocol.Login;
import sftp.client.protocol.checkClientPublicKey;
import sftp.client.protocol.getDirectoryAndFileList;
import sftp.client.ui.Entity.QueueFileInfo;
import sftp.client.ui.controls.AnimateProgressBar;
import sftp.client.ui.controls.FileListCell;
import sftp.client.ui.controls.LocalFileListCell;
import sftp.client.ui.controls.TransferFileProgressCell;
import sftp.lib.io.FileManage;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;
import sftp.lib.socket.Entity.UserLogin;
import sftp.lib.socket.FileHandleType;

/**
 * FXML Controller class
 *
 * @author Wensheng Yan
 */
public class MainFrameController extends BaseController {
    @FXML
    AnchorPane mainPane;
    @FXML
    ImageView bannerImage;
    @FXML
    MediaView bannerPlayer;
    @FXML
    ImageView logo;
    @FXML
    Label username;
    @FXML
    ToolBar userInfo;
    @FXML
    Button btnLogout;
    @FXML
    Button BtnSetting;
    @FXML
    Label usageTxt;
    @FXML
    TreeTableView localDirList, remoteDirList;
    @FXML
    ListView logview;
    @FXML
    AnimateProgressBar progress;
    @FXML
    HBox statusBar;
    @FXML
    Label dragView;
    @FXML
    AnchorPane browser;
    @FXML
    Label speed;
    @FXML
    TableView queuelist;
    @FXML
    Button btnClear;
    @FXML
    TabPane tab;
    
    //protected boolean isFinish = false;
    public Path currentDir;
    protected String remoteDir;
    private MainFrameController controller;
    private TransferDispatchThread dispatchThread;
    private ContextMenu localContextMenu;
    private ContextMenu remoteContextMenu;
    MenuItem Upload;
    MenuItem localCopy;
    MenuItem localPaste;
    MenuItem localDelete;
    MenuItem Download;
    MenuItem RemoteDelete;
    ObservableList<QueueFileInfo> queueData;
    ArrayList<String> copylists;//use for copy
    String copyDirectory;//use for copy
    AnimateProgressBar pb = new AnimateProgressBar();
    Timeline alive;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        controller = this;
        copylists = new ArrayList<>();
        
        Image logoImg = new Image(Utility.getFileFromResource(this, "images/logo.png"), 147, 57, false, true);
        logo.setImage(logoImg);
        
        Image setting = new Image(Utility.getFileFromResource(this, "images/Setting.png"), 32, 32, false, true);
        BtnSetting.setGraphic(new ImageView(setting));
        BtnSetting.setContentDisplay(ContentDisplay.LEFT);
        
        Image logout = new Image(Utility.getFileFromResource(this, "images/logout.png"), 32, 32, false, true);
        btnLogout.setGraphic(new ImageView(logout));
        btnLogout.setContentDisplay(ContentDisplay.LEFT);
        
        Image clear = new Image(Utility.getFileFromResource(this, "images/clear-icon.png"), 20, 20, false, true);
        btnClear.setGraphic(new ImageView(clear));
        btnClear.setContentDisplay(ContentDisplay.LEFT);
        btnClear.setDisable(true);
        
        Rectangle userInfo_rect = new Rectangle(500,50);
        userInfo_rect.setArcHeight(16.0);
        userInfo_rect.setArcWidth(16.0);
        userInfo.setClip(userInfo_rect);
        
        pb.setNeedAnimation(false);
        usageTxt.setGraphic(pb);
        usageTxt.setContentDisplay(ContentDisplay.TOP);
        
        browser.setOnDragOver((DragEvent event)->{
            Object obj = event.getDragboard().getContent(FileListCell.dataFormat);
            if(!(obj instanceof FileInfo)){
                return;
            }
            dragView.setVisible(true);
            event.acceptTransferModes(TransferMode.ANY);
            Point2D localPoint = browser.sceneToLocal(new Point2D(event.getSceneX(), event.getSceneY()));
            dragView.relocate(
                        (int) (localPoint.getX()),
                        (int) (localPoint.getY() - 30));
            event.consume();
        });
        
        final TreeItem<FileInfo> root =  new TreeItem<>(new FileInfo());

        try{
            DirectoryInfo info = FileManage.getDirectoryInfo(FileManage.getCurrentDirectory());
            currentDir = Paths.get(info.getDirectory());
            updateFilesList(root, info, currentDir.getParent() == null);
        }catch(NoSuchFileException e){
        }
        
        localDirList = setDirColumns(localDirList);
        localDirList.setRoot(root);
        localDirList.setRowFactory(
                new Callback<TreeTableView<FileInfo>, TreeTableRow<FileInfo>>() {
                    @Override
                    public TreeTableRow<FileInfo> call(TreeTableView<FileInfo> tableView) {
                        //final FileListCell row = new FileListCell(localDirList.getId(), dragView);
                        final LocalFileListCell row = new LocalFileListCell(localDirList.getId(), dragView, controller);
                        
                        localContextMenu = new ContextMenu();
                        
                        localDelete = new MenuItem("Delete");
                        Image delete = new Image(Utility.getFileFromResource(this, "images/delete.png"), 16, 16, false, true);
                        localDelete.setGraphic(new ImageView(delete));
                        localDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                Object[] items = localDirList.getSelectionModel().getSelectedItems().toArray();
                                for(Object item: items){
                                    localDirList.getRoot().getChildren().remove(item);
                                    TreeItem<FileInfo> treeItem = (TreeItem<FileInfo>)item;
                                    FileManage.deleteFileIfExists(currentDir.toAbsolutePath().toString() + File.separator + treeItem.getValue().getFileName());
                                }
                            }
                        });
                        
                        Upload = new MenuItem("Upload");
                        Image up = new Image(Utility.getFileFromResource(this, "images/upload.png"), 16, 16, false, true);
                        Upload.setGraphic(new ImageView(up));
                        Upload.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                Object[] items = localDirList.getSelectionModel().getSelectedItems().toArray();
                                for(Object item: items){
                                    TreeItem<FileInfo> treeItem = (TreeItem<FileInfo>)item;
                                    UploadFile(treeItem.getValue());
                                }
                            }
                        });
                        
                        localCopy = new MenuItem("Copy");
                        localPaste = new MenuItem("Paste");
                        Image copy = new Image(Utility.getFileFromResource(this, "images/copy.png"), 16, 16, false, true);
                        localCopy.setGraphic(new ImageView(copy));
                        Image paste = new Image(Utility.getFileFromResource(this, "images/paste.png"), 16, 16, false, true);
                        localPaste.setGraphic(new ImageView(paste));
                        localCopy.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                copylists.clear();
                                Object[] items = localDirList.getSelectionModel().getSelectedItems().toArray();
                                for(Object item: items){
                                    TreeItem<FileInfo> treeItem = (TreeItem<FileInfo>)item;
                                    String name = treeItem.getValue().getFileName();
                                    copyDirectory = currentDir.toAbsolutePath().toString();
                                    copylists.add(name);
                                }
                                Platform.runLater(() -> {
                                    localPaste.setDisable(false);
                                });
                            }
                        });
                        if(copylists.size() == 0){
                            localPaste.setDisable(true);
                        }else{
                           localPaste.setDisable(false); 
                        }
                        
                        localPaste.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                for(String s: copylists){
                                    String destPath = currentDir.toAbsolutePath().toString()+ File.separator + s;
                                    String srcPath = copyDirectory + File.separator + s;
                                    if(FileManage.checkFile(destPath)){
                                        destPath = currentDir.toAbsolutePath().toString()+ File.separator + Utility.getFileName(s) + "(copy)." +Utility.getFileExtension(s) ; 
                                    }
                                    try {
                                        FileManage.copyFile(srcPath, destPath);
                                    } catch (IOException ex) {
                                        Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                                copylists.clear();
                                DirectoryInfo info;
                                try {
                                    info = FileManage.getDirectoryInfo(currentDir.toAbsolutePath().toString());
                                    root.getChildren().clear();
                                    updateFilesList(root, info, currentDir.getParent() == null);
                                } catch (NoSuchFileException ex) {
                                    Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                               
                                Platform.runLater(() -> {
                                    localPaste.setDisable(true);
                                });
                            }
                        });
                        
                        localContextMenu.getItems().addAll(localCopy, localPaste,Upload, localDelete);
                        
                        Platform.runLater(() -> {
                            // only display context menu for non-null items:
                            row.contextMenuProperty().bind(
                                    Bindings.when(
                                            Bindings.and(
                                                    Bindings.isNotNull(row.itemProperty()),
                                                    row.itemProperty().isNotEqualTo(new FileInfo("..", true, 0, ""))
                                            )
                                    )
                                            .then(localContextMenu)
                                            .otherwise((ContextMenu)null)
                            );
                        });
                        
                        return row;
                    }
                });
        
        localDirList.setOnDragDropped((DragEvent event) -> {
            String strId = (String)event.getDragboard().getContent(FileListCell.IdFormat);
            if(strId.equals("remoteDirList")){
                Object obj = event.getDragboard().getContent(FileListCell.dataFormat);
                if(obj instanceof FileInfo){
                    FileInfo info = (FileInfo)obj;
                    downloadFile(info);
                }
            }
            event.consume();
        });
        

        
        localDirList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        /*
        localDirList.getSelectionModel().getSelectedIndices().addListener(
                new ListChangeListener<Integer>(){
                    @Override
                    public void onChanged(Change<? extends Integer> change){
                        Platform.runLater(() -> {
                            if(change.getList().size() >= 2){
                                if(!localContextMenu.getItems().contains(localDeleteAll)){
                                    localContextMenu.getItems().addAll(UploadAll, localDeleteAll);
                                    localContextMenu.getItems().removeAll(Upload, localDelete);
                                }
                            }else {
                                if(localContextMenu.getItems().contains(localDeleteAll)){
                                    localContextMenu.getItems().removeAll(UploadAll, localDeleteAll);
                                    localContextMenu.getItems().addAll(Upload, localDelete);
                                }
                            }
                        });
                    }
                }
        );
        */

        
        remoteDirList = setDirColumns(remoteDirList);
        remoteDirList.setRowFactory(
                new Callback<TreeTableView<FileInfo>, TreeTableRow<FileInfo>>() {
                    @Override
                    public TreeTableRow<FileInfo> call(TreeTableView<FileInfo> tableView) {
                        final FileListCell row = new FileListCell(remoteDirList.getId(), dragView);
                        remoteContextMenu = new ContextMenu();
                        RemoteDelete = new MenuItem("Delete");
                        Image delete = new Image(Utility.getFileFromResource(this, "images/delete.png"), 16, 16, false, true);
                        RemoteDelete.setGraphic(new ImageView(delete));
                        RemoteDelete.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                Object[] items = remoteDirList.getSelectionModel().getSelectedItems().toArray();
                                DirectoryInfo info = new DirectoryInfo(remoteDir);
                                for(Object item: items){
                                    TreeItem<FileInfo> treeItem = (TreeItem<FileInfo>)item;
                                    info.getFileLists().add(treeItem.getValue());
                                    remoteDirList.getRoot().getChildren().remove(item);
                                }
                                new DeleteFileThread(controller, application.getCurrentClient(), info).start();
                            }
                        });
                        
                        Download = new MenuItem("Download");
                        Image dl = new Image(Utility.getFileFromResource(this, "images/download.png"), 16, 16, false, true);
                        Download.setGraphic(new ImageView(dl));
                        Download.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                Object[] items = remoteDirList.getSelectionModel().getSelectedItems().toArray();
                                for(Object item: items){
                                    TreeItem<FileInfo> treeItem = (TreeItem<FileInfo>)item;
                                    downloadFile(treeItem.getValue());
                                }
                            }
                        });
                        remoteContextMenu.getItems().addAll(Download, RemoteDelete);
                        
                        Platform.runLater(() -> {
                            // only display context menu for non-null items:
                            row.contextMenuProperty().bind(
                                    Bindings.when(
                                            Bindings.and(
                                                    Bindings.isNotNull(row.itemProperty()),
                                                    row.itemProperty().isNotEqualTo(new FileInfo("..", true, 0, ""))
                                            )
                                    )
                                            .then(remoteContextMenu)
                                            .otherwise((ContextMenu)null)
                            );
                        });
                        
                        row.setOnMouseClicked(new EventHandler <MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                if(event.getButton().equals(MouseButton.PRIMARY)){
                                    if(event.getClickCount() == 2){
                                        String searchDir = remoteDir;
                                        if(!searchDir.endsWith("/")){
                                            searchDir += "/";
                                        }
                                        if(row.getItem().IsParentDirectory()){
                                            searchDir = URI.create(searchDir + "../").normalize().getPath();
                                        }else if(row.getItem().isDirectory()){
                                            searchDir += row.getItem().getFileName() + "/";
                                        }
                                        
                                        application.getCurrentClient().getDir(searchDir);
                                        new FileUpdateThread(controller, application.getCurrentClient()).start();
                                    }
                                }
                            }
                        });
                        
                        return row;
                    }
                }
        );
        remoteDirList.setOnDragDropped((DragEvent event) -> {
            if(application.getCurrentClient() != null && application.getCurrentClient().isAlive()){
                String strId = (String)event.getDragboard().getContent(FileListCell.IdFormat);
                if(strId.equals("localDirList")){
                    Object obj = event.getDragboard().getContent(FileListCell.dataFormat);
                    if(obj instanceof FileInfo){
                        FileInfo info = (FileInfo)obj;
                        UploadFile(info);
                    }
                }
            }
            event.consume();
        });
        remoteDirList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
     
        
        logview.getItems().add(0, "Welcome.");
        
        queuelist = setQueueColumns(queuelist);
        queueData = FXCollections.observableArrayList();
        queuelist.setItems(queueData);
        
        Platform.runLater(()->{
            if(application.getCurrentClient().userInfo != null){
                String strIcon = application.getCurrentClient().userInfo.getIcon();
                Image icon = null;
                if(!strIcon.equals("")){
                    byte[] byteIcon = sftp.lib.Utility.parseBase64Binary(strIcon);
                    icon = Utility.convertToFXImage(byteIcon);
                }else{
                    icon = new Image(Utility.getFileFromResource(this, "images/default-icon.jpg"));
                }
                ImageView view = new ImageView(icon);
                
                view.setId("user_icon");
                view.setFitHeight(32);
                view.setFitWidth(32);
                username.setGraphic(view);
                username.setContentDisplay(ContentDisplay.LEFT);
                UserLogin info = application.getCurrentClient().userInfo;
                if(!info.getFirstName().equals("") && !info.getLastName().equals("")){
                    username.setText(info.getFirstName() + " " + info.getLastName());
                }else{
                    username.setText(info.getUsername());
                }
                usageTxt.setText(Utility.getFileSize(info.getDiskUsage()) + "/" + Utility.getFileSize(info.getDiskCapacity()));
                pb.setProgress(info.getDiskUsage()/ (double)info.getDiskCapacity());
            }
            application.getCurrentClient().getEvents().add(new checkClientPublicKey(application.getCurrentClient()));
            application.getCurrentClient().getEvents().add(new GetSessionKey(application.getCurrentClient()));
            application.getCurrentClient().getEvents().add(new getDirectoryAndFileList(application.getCurrentClient()));
            new FileUpdateThread(controller, application.getCurrentClient()).start();
        });
        
        alive = new Timeline();
        alive.setCycleCount(Timeline.INDEFINITE);
        alive.getKeyFrames().add(
                new KeyFrame(Duration.seconds(10), new EventHandler<ActionEvent>(){
                    @Override
                    public void handle(ActionEvent event){
                        application.getCurrentClient().keepAlive();
                        //wake up client thread
                        synchronized(application.getCurrentClient()){
                            application.getCurrentClient().notify();
                        }
                        
                        if(!application.getCurrentClient().isAlive()){
                            alive.stop();
                            Platform.runLater(()->{
                                boolean gotoLogin = true;
                                
                                Action response = Dialogs.create()
                                        .title("Connection Abort")
                                                        .message("Connection Abort! Would you like to connect again?")
                                                        .showConfirm();
                                if(response == Dialog.Actions.YES){
                                    Client client = application.getCurrentClient();
                                    UserLogin info = client.userInfo;
                                    application.StartClient();
                                    client = application.getCurrentClient();
                                    try {
                                        Thread.sleep(200);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    if(client.isAlive()){
                                        client.getEvents().clear();
                                        client.getEvents().add(new Login(client, info.getUsername(), info.getPassword()));
                                        client.getEvents().add(new checkClientPublicKey(client));
                                        client.getEvents().add(new GetSessionKey(client));
                                        synchronized(client){
                                            client.notify();
                                        }
                                        gotoLogin = false;
                                        alive.play();
                                    }else{
                                        Dialogs.create()
                                            .title("Connection Failed")
                                                        .message("Please go to the login panel and login again.")
                                                        .showError();
                                    }
                                }
                                
                                if(gotoLogin){
                                    if(dispatchThread != null && dispatchThread.isAlive()){
                                        dispatchThread.close();
                                        NotifyThread();
                                        try {
                                            dispatchThread.join(500);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
  
                                    remoteDirList.setRoot(null);
                                    application.gotoLogin();
                                }
                            });
                        }
                    }}
                )
        );
        Platform.runLater(()->{
            alive.play();
        });
        
        tab.getSelectionModel().selectedItemProperty().addListener(                
            new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> ov, Tab oldTab, Tab newTab) {
                if(newTab.getText().equals("Log")){
                    if(logview.getItems().size() > 0){
                        btnClear.setDisable(false);
                    }else{
                        btnClear.setDisable(true);
                    }
                }else{
                    if(queueData.size() > 0){
                        btnClear.setDisable(false);
                    }else{
                        btnClear.setDisable(true);
                    }
                }
            }
        });
    }
    
    public TableView setQueueColumns(TableView tableview){
        TableColumn<QueueFileInfo, String> fileFromColumn = new TableColumn<>("From");
        fileFromColumn.setEditable(false);
        fileFromColumn.setMinWidth(150);
        fileFromColumn.setCellValueFactory(new PropertyValueFactory<QueueFileInfo,String>("from"));
        
        TableColumn<QueueFileInfo, String> fileToColumn = new TableColumn<>("To");
        fileToColumn.setEditable(false);
        fileToColumn.setMinWidth(150);
        fileToColumn.setCellValueFactory(new PropertyValueFactory<QueueFileInfo,String>("to"));
        
        TableColumn<QueueFileInfo, String> fileNameColumn = new TableColumn<>("File Name");
        fileNameColumn.setEditable(false);
        fileNameColumn.setMinWidth(200);
        fileNameColumn.setCellValueFactory(new PropertyValueFactory<QueueFileInfo,String>("filename"));
        
        TableColumn<QueueFileInfo, String> fileSizeColumn = new TableColumn<>("File Size");
        fileSizeColumn.setEditable(false);
        fileSizeColumn.setMinWidth(100);
        fileSizeColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<QueueFileInfo, String> param) ->{
                        long size = param.getValue().sizeProperty().get();
                        String strSize = Utility.getFileSize(size);
                        return new ReadOnlyStringWrapper(strSize);
                }
        );
        
        TableColumn<QueueFileInfo, Double> fileProgressColumn = new TableColumn<>("Progress");
        fileProgressColumn.setEditable(false);
        fileProgressColumn.setMinWidth(150);
        fileProgressColumn.setCellFactory(
                new Callback<TableColumn<QueueFileInfo,Double>, TableCell<QueueFileInfo,Double>>() {
                    @Override
                    public TableCell<QueueFileInfo, Double> call(TableColumn<QueueFileInfo, Double> param) {
                        TableCell<QueueFileInfo, Double> row = new TransferFileProgressCell();
                        return row;
                    }
                }
        );
        fileProgressColumn.setCellValueFactory(new PropertyValueFactory<QueueFileInfo,Double>("progress"));
        
        TableColumn<QueueFileInfo, String> fileSpeedColumn = new TableColumn<>("Speed");
        fileSpeedColumn.setEditable(false);
        fileSpeedColumn.setMinWidth(150);
        fileSpeedColumn.setCellValueFactory(new PropertyValueFactory<QueueFileInfo,String>("speed"));
        
        tableview.getColumns().setAll(fileFromColumn, fileToColumn, fileNameColumn, fileSizeColumn, fileProgressColumn, fileSpeedColumn);
        return tableview;
    }
    
    public TreeTableView setDirColumns(TreeTableView treeview){
        TreeTableColumn<FileInfo, String> filenameColumn = new TreeTableColumn<>("File Name");
        filenameColumn.setEditable(false);
        filenameColumn.setMinWidth(130);
        filenameColumn.setCellValueFactory(
            (TreeTableColumn.CellDataFeatures<FileInfo, String> param) -> 
                new ReadOnlyStringWrapper(param.getValue().getValue().getFileName())
        );
        
        TreeTableColumn<FileInfo, String> filesizeColumn = new TreeTableColumn<>("File Size");
        filesizeColumn.setEditable(false);
        filesizeColumn.setMinWidth(130);
        filesizeColumn.setCellValueFactory(
            (TreeTableColumn.CellDataFeatures<FileInfo, String> param) -> { 
               long size = param.getValue().getValue().getFileSize();
               String strSize = "";
               if(!param.getValue().getValue().isDirectory()){
                   strSize = Utility.getFileSize(size);
               }
               return new ReadOnlyStringWrapper(strSize);
             }
        );
        
        TreeTableColumn<FileInfo, String> lastModifiedColumn = new TreeTableColumn<>("last Modified");
        lastModifiedColumn.setEditable(false);
        lastModifiedColumn.setMinWidth(130);
        lastModifiedColumn.setCellValueFactory(
            (TreeTableColumn.CellDataFeatures<FileInfo, String> param) -> 
                new ReadOnlyStringWrapper(param.getValue().getValue().getLastModified())
        );
        treeview.getColumns().setAll(filenameColumn, filesizeColumn, lastModifiedColumn);
        treeview.setShowRoot(false);
        return treeview;
    }
    /*
    public void onBtnConnectClicked(MouseEvent event){
        if(connected){
            connected = false;
            BtnConnect.setText("Connect");
            try {
                application.getCurrentClient().close();
                synchronized(application.getCurrentClient()){
                    application.getCurrentClient().notify();
                }
                if(dispatchThread != null && dispatchThread.isAlive()){
                    dispatchThread.close();
                    synchronized(dispatchThread){
                        dispatchThread.notify();
                    }
                    dispatchThread.join(500);
                }
                application.getCurrentClient().join();//wait for thread to die.
                remoteDirList.setRoot(null);

            } catch (InterruptedException ex) {
                Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            if(application.getCurrentClient() == null || !application.getCurrentClient().isAlive()){
                connected = true;
                BtnConnect.setText("DisConnect");
                application.StartClient();
                new FileUpdateThread(controller, application.getCurrentClient()).start();
            }else{
                Dialogs.create()
                    .message("The previous connection is still alive!")
                    .showError();
            }
        }
    }
    */
    
    public void onBtnShortCutAllClicked(MouseEvent event){
        application.getCurrentClient().getDir("/");
        new FileUpdateThread(controller, application.getCurrentClient()).start();
    }
    
    public void onBtnShortCutFileClicked(MouseEvent event){
        application.getCurrentClient().getDir("/Document");
        new FileUpdateThread(controller, application.getCurrentClient()).start();
    }   
    
    public void onBtnShortCutPicClicked(MouseEvent event){
        application.getCurrentClient().getDir("/Picture");
        new FileUpdateThread(controller, application.getCurrentClient()).start();
    }  
    
    public void onBtnShortCutMusicClicked(MouseEvent event){
        application.getCurrentClient().getDir("/Music");
        new FileUpdateThread(controller, application.getCurrentClient()).start();
    } 
    
    public void onBtnShortCutVideoClicked(MouseEvent event){
        application.getCurrentClient().getDir("/Video");
        new FileUpdateThread(controller, application.getCurrentClient()).start();
    } 
    
    public void onBtnSettingClicked(MouseEvent event){
        application.gotoSetting();
    }
    
    public void onBtnClear(MouseEvent event){
        String text = tab.getSelectionModel().selectedItemProperty().get().getText();
        if(text.equals("Log")){
            logview.getItems().clear();
        }
        else{
            boolean needClear = true;
            if(dispatchThread != null && dispatchThread.isAlive()){
                Action response = Dialogs.create()
                        .message("There still have files in the transfer list, Do you want to cancel them?")
                        .showConfirm();
                if(response == Dialog.Actions.YES){
                    try {
                        dispatchThread.close();
                        synchronized(dispatchThread){
                            dispatchThread.notify();
                        }
                        dispatchThread.join(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    needClear = false;
                }
            }
            if(needClear){
                queueData.clear();
                
            }
        }
        
        btnClear.setDisable(true);
    }
    
    public void onBtnlogoutClicked(MouseEvent event){
            try {
                if(alive != null){
                    alive.stop();
                }
                application.getCurrentClient().close();
                synchronized(application.getCurrentClient()){
                    application.getCurrentClient().notify();
                }
                if(dispatchThread != null && dispatchThread.isAlive()){
                    dispatchThread.close();
                    NotifyThread();
                    dispatchThread.join(500);
                }
                application.getCurrentClient().join();//wait for thread to die.
                remoteDirList.setRoot(null);
                application.gotoLogin();
            } catch (InterruptedException ex) {
                Logger.getLogger(MainFrameController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public void updateFilesList(TreeItem<FileInfo> root, DirectoryInfo info, boolean isRoot){
        if(!isRoot){
            Image icon = new Image(Utility.getFileFromResource(this, "images/folder-prev.jpg"), 20, 20, false, true);
            root.getChildren().add(new TreeItem<>(new FileInfo("..", true, 0, ""), new ImageView(icon)));
        }
        info.getFileLists().stream().forEach((file)->{
                    Image fileicon = null;
                    //filter hidden files and directories
                    if(!file.getFileName().startsWith(".")){
                        fileicon = Utility.getFileIcon(this, file);
                        root.getChildren().add(new TreeItem<>(file, new ImageView(fileicon)));
                    }
        });
    }
    
    public void transferFile(FileInfo info, FileHandleType type){
        if(dispatchThread == null || !dispatchThread.isAlive()){
            queueData.clear();
            btnClear.setDisable(false);
        }
        if(FileTransferManageCenter.FileDetail(info.getFileName()) != null){
            Platform.runLater(()->{
                Dialogs.create()
                        .message(String.format("File %s already in lists!", info.getFileName()))
                        .showError();
            });
            return;
        }
        if(type == FileHandleType.put){
            queueData.add(new QueueFileInfo("Client: " + currentDir.toAbsolutePath().toString(), "Server: " + remoteDir, info.getFileName(), info.getFileSize()));
            FileTransferManageCenter.add(info,currentDir.toAbsolutePath().toString(), remoteDir, FileHandleType.put);
        }else{
            queueData.add(new QueueFileInfo("Server: " + remoteDir, "Client: " + currentDir.toAbsolutePath().toString(), info.getFileName(), info.getFileSize()));
            FileTransferManageCenter.add(info, remoteDir, currentDir.toAbsolutePath().toString(), FileHandleType.get);
        }
        
        if(dispatchThread == null || !dispatchThread.isAlive()){
            dispatchThread = new TransferDispatchThread(controller, application.getCurrentClient());
            dispatchThread.start();
        }else{
            NotifyThread();
        }
    }
    
    public void downloadFile(FileInfo info){
        transferFile(info, FileHandleType.get);
    }

    private void UploadFile(FileInfo info) {
        transferFile(info, FileHandleType.put);
    }
    
    public void NotifyThread(){
        synchronized(dispatchThread){
            dispatchThread.notify();
        }
    }
}
