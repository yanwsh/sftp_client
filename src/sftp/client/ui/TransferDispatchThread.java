/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.nio.file.NoSuchFileException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TreeItem;
import javafx.util.Duration;
import sftp.client.Client;
import sftp.client.FileTransferManageCenter;
import sftp.client.Utility;
import sftp.client.ui.Entity.QueueFileInfo;
import sftp.client.ui.Entity.TransferFile;
import sftp.client.ui.controls.FileListCell;
import sftp.lib.Console;
import sftp.lib.io.FileManage;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;
import sftp.lib.socket.Entity.TransferFileInfo;
import sftp.lib.socket.FileHandleType;

/**
 * @file TransferDispatchThread.java
 * @author Wensheng Yan
 * @date Apr 11, 2014
 */
public class TransferDispatchThread extends BaseThread {
    private static final int MAXThread = 4;
    Timeline speedline;
    //protected ArrayList<TransferFileCheckThread> checkThreads;
    protected ArrayList<TransferFile> Transferfiles;
    protected Queue<Client> worklists;
    protected Queue<Client> availablelists;
    long totalLastRecSize;
    double totalProgress;
    int finishCount;
    boolean needNotify;
    boolean remoteUpdate;
    boolean localUpdate;
    
    public TransferDispatchThread(MainFrameController controller, Client client){
        super(controller, client);
        //checkThreads = new ArrayList<>();
        Transferfiles = new ArrayList<>();
        needNotify = false;
        remoteUpdate = false;
        localUpdate = false;
        worklists = new ArrayDeque<>();
        availablelists = new ArrayDeque<>();
        availablelists.offer(client);
    }
    

    
    @Override
    public void run() { 
        Console.println("MSG: Thread id: " + this.getId() + " start.");
        finishCount = 0;
        //update for transfer info and total info every one seconds.
        Platform.runLater(()->{
            controller.speed.setVisible(true);
            controller.speed.setText("0.00B/s");
            speedline = new Timeline();
            speedline.setCycleCount(Timeline.INDEFINITE);
            speedline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>(){
                    @Override
                    public void handle(ActionEvent event){
                        totalLastRecSize = 0;
                        totalProgress = 0;
                        needNotify = false;
                        //update each transfer info
                        controller.queueData.forEach((QueueFileInfo qfi)-> {
                             String filename = qfi.filenameProperty().get();
                             TransferFileInfo cfi = FileTransferManageCenter.FileDetail(filename);
                             if(cfi != null){
                                double progress = cfi.getProgress();
                                qfi.progressProperty().set(progress);
                                long lastRecSize = cfi.getLastRecSize();
                                String speed = "";
                                boolean isError = false;
                                if(cfi.getMessage().equals("")){
                                    speed = Utility.getFileSize(lastRecSize) + "/s";
                                }else{
                                    speed = cfi.getMessage();
                                    isError = true;
                                }
                                qfi.speedProperty().set(speed);
                                totalProgress += cfi.getProgress();
                                totalLastRecSize += cfi.getLastRecSize();
                                cfi.setLastRecSize(0);
                                if(progress == 1.0 || isError){
                                    Object[] objs = Transferfiles.stream().filter((TransferFile tf)-> {
                                        return tf.getFilename().equals(filename);
                                    }).toArray();
                                    TransferFile tf = null;
                                    if(objs != null && objs.length > 0) tf = (TransferFile)objs[0];
                                    Client c = tf.getClient();
                                    if(c == null || (c.getState() == Thread.State.WAITING)){
                                        Console.println("MSG: client id: " + c.getId() + " suspended.");
                                        worklists.remove(c);
                                        availablelists.add(c);
                                        if(cfi.getType() == FileHandleType.get){
                                            if(cfi.getTo().equals(controller.currentDir.toAbsolutePath().toString())){
                                                localUpdate = true;
                                            }
                                        }else{
                                           if(cfi.getTo().equals(controller.remoteDir)){
                                                remoteUpdate = true;
                                            }     
                                        }
                                        FileTransferManageCenter.remove(filename);
                                        if(!isError){
                                            qfi.speedProperty().set("");
                                        }
                                        finishCount++;
                                        totalProgress -= progress;
                                        needNotify = true;
                                    }
                                }else if(progress > 1.0){
                                    try {
                                        throw new Exception("Invalid progress Error.");
                                    } catch (Exception ex) {
                                        Logger.getLogger(TransferDispatchThread.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                             }
                        });
                        
                        totalProgress = (totalProgress + finishCount * 1.0) / controller.queuelist.getItems().size();
                        controller.speed.setText(Utility.getFileSize(totalLastRecSize) + "/s");
                        controller.progress.setProgress(totalProgress);
                        
                        //update log
                        while(!Console.messageList.isEmpty())
                            controller.logview.getItems().add(0, Console.messageList.poll());
                        if(needNotify){
                            controller.NotifyThread();
                            UpdateWindowList();
                            new UserDiskInfoUpdateThread(controller, client).start();
                        }
                    }
                })
            );
            speedline.play();
        });
        
        while(!needClose){
            while(FileTransferManageCenter.size() > 0){
                String file = FileTransferManageCenter.getFile();
                TransferFileInfo info = FileTransferManageCenter.FileDetail(file);
                Client currentclient = null;
                if(availablelists.size() == 0){
                    //need more threads
                    if(worklists.size() <= MAXThread){
                        currentclient = new Client(controller.application.conf, Client.ClientType.Assistant);
                        currentclient.fork(this.client);
                        currentclient.registerThread();
                        currentclient.start();
                    }
                }else{
                    //if it is available
                    currentclient = availablelists.poll();
                }
                
                if(currentclient != null){
                    FileTransferManageCenter.pollFile();
                    Console.println("MSG: client id: " + currentclient.getId() + " start.");
                    if(info.getType() == FileHandleType.put){
                        currentclient.putFile(info.getFilename(), info.getTo(), info.getFrom());
                    }
                    else{
                        currentclient.getFile(info.getFilename(), info.getTo(), info.getFrom());
                    }
                    worklists.add(currentclient);
                    FileTransferManageCenter.addWork(file);
                    Transferfiles.add(new TransferFile(currentclient, file));
                    //notify client to start
                    synchronized(currentclient){
                        currentclient.notify();
                    }
                    /*
                    TransferFileCheckThread tfc = new TransferFileCheckThread(controller, currentclient, this, file);
                    checkThreads.add(tfc);
                    tfc.start();
                            */
                }
            }
        
            //two possible conditions, one: up to max thread, two: all the thread dispatched.
            synchronized(this){
                try {
                    wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(TransferDispatchThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
            //invoke thread, three possible conditions:
            //One: something add to the FileTransferManageCenter. 
            //Two: File transfer finished.
            //Three: Thread need to die.
            //If file transfer finished, another thread will help to remove client and add it to available.
            //we only need to check the number of file count in FileTransferManageCenter
            if(FileTransferManageCenter.size() == 0 && worklists.size() == 0) 
                break;
        }
        
        //finish everything, now threads except main thread need to be closed.
        Platform.runLater(()->{
            //check whether other program need this thread to die.
            if(needClose){
                //clear speed
                controller.queueData.forEach((QueueFileInfo qfi)-> {
                    qfi.speedProperty().set("");
                });  
                controller.progress.setMessage("User Stop");
            }
            else{
                //clear speed, set progress to 1.0
//                controller.queueData.forEach((QueueFileInfo qfi)-> {
//                    String filename = qfi.filenameProperty().get();
//                    
//                    TransferFileInfo tfi = FileTransferManageCenter.FileDetail(filename);
//                    if(tfi != null && !tfi.getMessage().equals("")){
//                        qfi.progressProperty().set(1.0);
//                        qfi.speedProperty().set("");
//                    }
//                    FileTransferManageCenter.remove(filename);
//                });
                controller.progress.setProgress(1.0);
                controller.progress.setMessage("Finish");
            }
            controller.speed.setVisible(false);
            speedline.stop(); 
            
            //update log
            while(!Console.messageList.isEmpty())
                controller.logview.getItems().add(0, Console.messageList.poll());
        });
        
        //close all checkThread if it still alive
        /*
        for(TransferFileCheckThread tfc: checkThreads){
            if(!tfc.isAlive()) continue;
            tfc.close();
            synchronized(tfc){
                tfc.notify();
            }
            try {
                tfc.join(500);//wait for thread to die.
            } catch (InterruptedException ex) {
                Logger.getLogger(TransferDispatchThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        */
        closeQueue(availablelists); 
        
        //In emergency closed condition, it is possible for clients still in worklists. We need to close it.
        if(needClose){
            closeQueue(worklists);            
        }
        
        Console.println("MSG: Thread id: " + this.getId() + " die.");
    }
    
    private void closeQueue(Queue<Client> lists){
        lists.forEach((Client c) ->{
            if(c.getClienttype() !=  Client.ClientType.Main){
                c.close();
                synchronized(c){
                    c.notify();
                }
                try {
                    c.join(500);//wait for thread to die.
                } catch (InterruptedException ex) {
                    Logger.getLogger(TransferDispatchThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                Console.println("MSG: client id: " + c.getId() + " die.");
            }        
        });
    }
    
    private void UpdateWindowList(){
            try {
                if(localUpdate){
                        DirectoryInfo info = FileManage.getDirectoryInfo(controller.currentDir.toAbsolutePath().toString());
                        TreeItem<FileInfo> currentRoot = controller.localDirList.getRoot();
                        currentRoot.getChildren().clear();
                        controller.updateFilesList(currentRoot, info, controller.currentDir.getParent() == null);
                }
                
                if(remoteUpdate){
                        client.getDir(controller.remoteDir);
                        new FileUpdateThread(controller, client).start();
                }
                
            } catch (NoSuchFileException ex) {
                Logger.getLogger(FileListCell.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}
