/*
 * Copyright (C) 2014 wensheng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Client;
import sftp.client.ProtocolState;
import sftp.client.Utility;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.UserLogin;

/**
 *
 * @author wensheng
 */
public class DeleteFileThread extends BaseThread {
    private DirectoryInfo info;
    
    public DeleteFileThread(MainFrameController controller, Client client, DirectoryInfo info){
        super(controller, client);
        this.info = info;
    }
    
    @Override
    public void run() {
        client.DeleteFile(info);
        super.run();
        client.updatestate = ProtocolState.Unknown;
        
        if(!client.isAlive()){
            Platform.runLater(()->{
                Dialogs.create()
                        .message("Fail to connect to server!")
                        .showError();
                controller.remoteDirList.setRoot(null);
            });
            return;
        }
        
        while(client.updatestate == ProtocolState.Unknown){
            synchronized(client){
                try {
                    client.wait(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(UserDiskInfoUpdateThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        if(client.updatestate == ProtocolState.Error){
            Platform.runLater(()->{
                Dialogs.create()
                        .message("Delete file(s) failed!")
                        .showError();
            });
        }else{
            Platform.runLater(()->{
                UserLogin u = client.userInfo;
                controller.pb.setProgress(u.getDiskUsage()/ (double)u.getDiskCapacity());
                controller.usageTxt.setText(Utility.getFileSize(u.getDiskUsage()) + "/" + Utility.getFileSize(u.getDiskCapacity()));
            });
        }
    }
}
