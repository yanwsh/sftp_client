/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.nio.file.NoSuchFileException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TreeItem;
import javafx.util.Duration;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Client;
import sftp.client.FileTransferManageCenter;
import sftp.client.Main;
import sftp.client.Utility;
import sftp.client.protocol.GetFile;
import sftp.client.ui.controls.FileListCell;
import sftp.lib.Console;
import sftp.lib.io.FileManage;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;
import sftp.lib.socket.Entity.TransferFileInfo;
import sftp.lib.socket.FileHandleType;

/**
 * @file FileGetThread.java
 * @author Wensheng Yan
 * @date Apr 6, 2014
 */
@Deprecated
public class TransferFileCheckThread extends BaseThread {
    TransferDispatchThread dispatchThread;
    String filename;
    
    public TransferFileCheckThread(MainFrameController controller, Client client, TransferDispatchThread dispatchThread, String filename){
        super(controller, client);
        this.dispatchThread = dispatchThread;
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }
    
    @Override
    public void run() {
        super.run();
        
        if(!client.isAlive()){
            controller.logview.getItems().add(0, "client error.");
            dispatchThread.worklists.remove(client);
            return;
        } 
        
        while(client.getState() != Thread.State.WAITING && !needClose){
            synchronized(client){
                try {
                    //wait timeout is 5s
                    client.wait(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FileUpdateThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        if(needClose) return;
        
        TransferFileInfo tfi = FileTransferManageCenter.FileDetail(filename);
        //update list and log
        Platform.runLater(()-> {
            try {
                if(tfi.getType() == FileHandleType.get){
                    if(tfi.getTo().equals(controller.currentDir.toAbsolutePath().toString())){
                        DirectoryInfo info = FileManage.getDirectoryInfo(controller.currentDir.toAbsolutePath().toString());
                        TreeItem<FileInfo> currentRoot = controller.localDirList.getRoot();
                        currentRoot.getChildren().clear();
                        controller.updateFilesList(currentRoot, info, controller.currentDir.getParent() == null);
                    }
                }else{
                    if(tfi.getTo().equals(controller.remoteDir)){
                        DirectoryInfo dirInfo = client.getDirInfo();
                        controller.remoteDir = dirInfo.getDirectory();
                        boolean isRoot = false;
                        if(controller.remoteDir.equals("/")){
                            isRoot = true;
                        }
                        final TreeItem<FileInfo> root =  new TreeItem<>(new FileInfo());
                        root.getChildren().clear();
                        controller.remoteDirList.setRoot(root);
                        controller.updateFilesList(root, dirInfo, isRoot);
                    }
                }
                while(!Console.messageList.isEmpty()){
                    controller.logview.getItems().add(0, Console.messageList.poll());
                }
            } catch (NoSuchFileException ex) {
                Logger.getLogger(FileListCell.class.getName()).log(Level.SEVERE, null, ex);
            }
        });


        synchronized(dispatchThread){
            //update DispatchThread lists
            Console.println("MSG: client id: " + client.getId() + " suspended.");
            dispatchThread.worklists.remove(client);
            dispatchThread.availablelists.add(client);
            //dispatchThread.checkThreads.remove(this);
            //notify DispatchThread
            dispatchThread.notify();
        }
    }
}
