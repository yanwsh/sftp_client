/*
* Copyright (C) 2014 wensheng
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package sftp.client.ui;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Client;
import sftp.client.Config;
import sftp.client.Main;
import sftp.client.ProtocolState;
import sftp.client.protocol.Login;
import sftp.lib.socket.Entity.UserLogin;

/**
 *
 * @author wensheng
 */
public class LoginThread extends Thread {
    Main application;
    String username;
    String password;
    Client client;
    LoginController controller;
    
    public LoginThread(Main application, LoginController controller, String name, String pass){
        this.application = application;
        this.username = name;
        this.password = pass;
        this.controller = controller;
    }
    
    @Override
    public void run() {
        application.StartClient();
        client = application.getCurrentClient();
        if(!client.isAlive()){
            Platform.runLater(()->{
                Dialogs.create()
                        .message("Fail to connect to server!")
                        .showError();
            });
            Platform.runLater(()->{
                if(controller != null){
                    controller.isShowing.set(false);
                }
            });
            return;
        }
        
        client.getEvents().add(new Login(client, username, password));
        
        //wake up client thread
        synchronized(client){
            client.notify();
        }
        
        while(client.loginstate == ProtocolState.Unknown){
            synchronized(client){
                try {
                    client.wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(FileUpdateThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        if(client.loginstate == ProtocolState.Error){
            Platform.runLater(()->{
                Dialogs.create()
                        .message(client.getErrorMessage())
                        .showError();
            });
        }else{
            ArrayList<UserLogin> users = application.conf.getSavedUsers();
            UserLogin userInfo = null;
            boolean contains = false;
            for(UserLogin u: users){
                if(u.getUsername().equals(username)){
                    userInfo = u;
                    contains = true;
                    break;
                }
            }
            if(userInfo == null){
                userInfo = new UserLogin(username);
            }
            if(controller != null){
                if(controller.savePass.isSelected()){
                    userInfo.setPassword(password);
                }
                else{
                    userInfo.setPassword("");
                }
                if(controller.autoLogin.isSelected()){
                    application.conf.setAutoUsername(username);
                }
                else{
                    application.conf.setAutoUsername("");
                }
            }
            application.conf.setLastUsername(username);
            userInfo.setIcon(client.userInfo.getIcon());
            if(!contains){
                users.add(userInfo);
            }
            Config.SaveConfig(application.conf);
            if(controller != null)
                application.gotoMainFrame();
        }
        Platform.runLater(()->{
            if(controller != null)
                controller.isShowing.set(false);
        });
    }
}
