/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sftp.client.Main;
import sftp.client.ui.controls.WindowButtons;

/**
 * @file BaseController.java
 * @author Wensheng Yan
 * @date Mar 29, 2014
 */
public class BaseController implements Initializable {
    protected Main application;
    protected Stage stage;
    protected Stage parentStage;
    protected double mouseDragOffsetX = 0;
    protected double mouseDragOffsetY = 0;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    public void setApp(Main application, Stage stage){
        this.application = application;
        this.stage = stage;
    } 
    
    public void setParentStage(Stage stage){
        this.parentStage = stage;
    }
    
    public void onGragPaneMousePressed(MouseEvent event){
        mouseDragOffsetX = event.getSceneX();
        mouseDragOffsetY = event.getSceneY();
    }
    
    public void onGragPaneMouseDragged(MouseEvent event){
        stage.setX(event.getScreenX()-mouseDragOffsetX);
        stage.setY(event.getScreenY()-mouseDragOffsetY);
    }

    
    public static Initializable replaceSceneContent(Stage stage, String fxml, boolean windowbutton, Main main) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = BaseController.class.getResourceAsStream("fxml/" + fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(BaseController.class.getResource("fxml/" + fxml));
        AnchorPane page;
        try {
            page = (AnchorPane) loader.load(in);
        } finally {
            in.close();
        }
        
        if(windowbutton && !Main.isApplet){
            // add close min max
            final WindowButtons windowButtons = new WindowButtons(stage, main);
            windowButtons.setLayoutX(page.getPrefWidth() - 75);
            windowButtons.setLayoutY(5);
            windowButtons.setPadding(new Insets(0, 0, 0, 0));
            page.getChildren().add(windowButtons);
        }
                
        Scene scene = new Scene(page, page.getPrefWidth(), page.getPrefHeight());
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.sizeToScene();
        return (Initializable) loader.getController();
    }
}
