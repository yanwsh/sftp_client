/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Config;
import sftp.client.Utility;
import sftp.lib.crypto.MD5;
import sftp.lib.socket.Entity.UserLogin;

/**
 * FXML Controller class
 *
 * @author Wensheng Yan
 */
public class SettingController extends BaseController {
    @FXML
    PasswordField passwordField;
    @FXML
    TextField FirstnameField;
    @FXML
    TextField LastnameField;
    @FXML
    ImageView usricon;
    @FXML
    HBox iconbox;
    @FXML
    CheckBox showLogin;
    
    boolean iconChanged;
    String extension;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iconChanged = false;
        Platform.runLater(()->{
            UserLogin info = application.getCurrentClient().userInfo;
            String strIcon = info.getIcon();
            Image img = null;
            if(!strIcon.equals("")){
                byte[] byteIcon = sftp.lib.Utility.parseBase64Binary(strIcon);
                img = Utility.convertToFXImage(byteIcon);
            }else{
                img = new Image(Utility.getFileFromResource(this, "images/default-icon.jpg"), 50, 50, false, true);
            }
            usricon.setImage(img);
            passwordField.setText("********");
            passwordField.setOnMouseClicked((MouseEvent event)->{
                passwordField.setText("");
            });
            FirstnameField.setText(info.getFirstName());
            LastnameField.setText(info.getLastName());
            if(!application.conf.getAutoUsername().equals("")){
                showLogin.setSelected(true);
            }
        });
        
        iconbox.setOnDragOver(new EventHandler <DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if(db.hasFiles()){
                    event.acceptTransferModes(TransferMode.COPY);
                }
                 
                event.consume();
            }
        });
        
        iconbox.setOnDragDropped(new EventHandler <DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if(db.hasFiles()){
                    try {
                        File file = db.getFiles().get(0);
                        if(checkImage(file)){
                            String absolutePath = file.toURI().toURL().toString();
                            Image img = new Image(absolutePath, 50, 50, false, true);
                            usricon.setImage(img);
                            extension = Utility.getFileExtension(file.getName());
                            iconChanged = true;
                        }
                        event.setDropCompleted(true);
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(SettingController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    event.setDropCompleted(false);
                }
 
                event.consume();                
            }
        });
    }    
    
    private boolean checkImage(File file){
        extension = Utility.getFileExtension(file.getName());
        if(!extension.equals("gif") && !extension.equals("jpg") && !extension.equals("png")){
            Dialogs.create()
                    .message("Not a valid image file.")
                    .showError();
            return false;
        }
        return true;
    }
    
    public void onBtnChangedIconClicked(MouseEvent event){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose an image");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("gif", "*.gif")
            );
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
            );           
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            Platform.runLater(()->{
                try {
                    if(checkImage(file)){
                        String absolutePath = file.toURI().toURL().toString();
                        Image img = new Image(absolutePath, 50, 50, false, true);
                        usricon.setImage(img);
                        iconChanged = true;
                    }
                } catch (MalformedURLException ex) {
                    Logger.getLogger(SettingController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
    }
    
    public void onBtnKeyChangeClicked(MouseEvent event){
        application.conf.setKey(Config.generateRSAKey());
        Config.SaveConfig(application.conf);
        Dialogs.create()
            .message("Key changed successfully, new key will be used on next login!")
            .showInformation();
    }
    
    public void onBtnCloseClicked(MouseEvent event){
        stage.close();
    }
    
    public void onBtnOKClicked(MouseEvent event){
        Platform.runLater(()->{
            boolean changed = false;
            String firstname = FirstnameField.getText();
            String lastname = LastnameField.getText();
            UserLogin info = application.getCurrentClient().userInfo;
            if(!info.getFirstName().equals(firstname) || !info.getLastName().equals(lastname)){
                info.setFirstName(firstname);
                info.setLastName(lastname);
                changed = true;
            }
            String password = passwordField.getText();
            if(!password.equals("********") && !password.equals("")){
                MD5 md5 = new MD5();
                info.setPassword(md5.EncryptToString(password));
                changed = true;
            }else{
                info.setPassword("");
            }
            
            if(iconChanged){
                Image img = usricon.getImage();
                try {
                    byte[] ImgByte = Utility.ConvertImageToByteArray(img, extension);
                    String iconStr = sftp.lib.Utility.printBase64Binary(ImgByte);
                    info.setIcon(iconStr);
                    changed = true;
                } catch (Exception ex) {
                    Dialogs.create()
                        .message("Save Image error, please upload another image.")
                        .showError();
                    Logger.getLogger(SettingController.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            }
            if(changed)
                new UserInfoChangeThread(application.getCurrentClient(), stage, info).start();
            
            if(showLogin.isSelected()){
                application.conf.setAutoUsername(application.getCurrentClient().userInfo.getUsername());
            }else{
                application.conf.setAutoUsername("");
            }
            
            if(changed && iconChanged){
                ArrayList<UserLogin> users = application.conf.getSavedUsers();
                for(UserLogin user: users){
                    if(user.getUsername().equals(info.getUsername())){
                        user.setIcon(info.getIcon());
                        break;
                    }
                }
            }
            Config.SaveConfig(application.conf);
            
            Node node = this.parentStage.getScene().getRoot().lookup("#username");
            Label username = (Label)node;

            if(!firstname.equals("") && !lastname.equals("")){
                username.setText(FirstnameField.getText() + " " + LastnameField.getText());
            }
            node = this.parentStage.getScene().getRoot().lookup("#user_icon");
            ImageView view = (ImageView)node;
            
            view.setImage(usricon.getImage());
            
            if(!changed){
                this.stage.close();
            }
        });
    }
}
