/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import sftp.client.Client;
import sftp.lib.Console;

/**
 * @file BaseThread.java
 * @author Wensheng Yan
 * @date Apr 6, 2014
 */
public class BaseThread extends Thread {
    protected MainFrameController controller;
    protected boolean isScheduled;
    protected Client client;
    protected boolean needClose;
    
    public BaseThread(MainFrameController controller, Client client){
        this.controller = controller;
        this.client = client;
        this.isScheduled = false;
        this.needClose = false;
    }
    
    public void close(){
        this.needClose = true;
    }
    
    @Override
    public void run() {
        //wake up client thread
        synchronized(client){
            client.notify();
        }
    }
    
    public void updateMessage(){
        if(!Console.messageList.isEmpty() && isScheduled == false){
            isScheduled = true;
            Platform.runLater(()->{
                while(!Console.messageList.isEmpty())
                    controller.logview.getItems().add(0, Console.messageList.poll());
                isScheduled = false;
            });
        }else{
            synchronized(client){
                try {
                    //wait timeout is 5s
                    client.wait(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FileUpdateThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
