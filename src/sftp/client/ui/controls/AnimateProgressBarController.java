/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui.controls;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Wensheng Yan
 */
public class AnimateProgressBarController implements Initializable {
    @FXML
    ProgressBar progress;
    @FXML
    Label progressText;
    @FXML
    StackPane stackpane;
    
    DoubleProperty dp1 = new SimpleDoubleProperty(0.1000);
    DoubleProperty dp2 = new SimpleDoubleProperty(0.7500);
    BooleanProperty needAnimation = new SimpleBooleanProperty(true);
    Timeline flash;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        flash = new Timeline(
            new KeyFrame(Duration.seconds(0),    new KeyValue(dp1, 0.1000)),
            new KeyFrame(Duration.seconds(0),    new KeyValue(dp2, 0.7500)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(dp1, 3.1000)),
            new KeyFrame(Duration.seconds(1),    new KeyValue(dp2, 3.7500))
        );
        flash.setCycleCount(Timeline.INDEFINITE);
        
        Platform.runLater(()->{
            Node bar = progress.lookup(".bar");
            bar.styleProperty().bind(
                new SimpleStringProperty("-fx-background-color: linear-gradient(")
                    .concat("from ")
                    .concat(dp1)
                    .concat("em 0.75em to ")
                    .concat(dp2)
                    .concat("em 0px,")
                    .concat("repeat,")
                    .concat("-fx-accent 0%,")
                    .concat("-fx-accent 49%,")
                    .concat("derive(-fx-accent, 30%) 50%,")
                    .concat("derive(-fx-accent, 30%) 99%")
                    .concat(");")
            );
        });
        
        progress.progressProperty().addListener(
                new ChangeListener<Number>(){
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if(needAnimation.get()){
                            if(progress.getProgress() > 0 && progress.getProgress() < 1){
                                if(flash.getStatus() != Animation.Status.RUNNING)
                                    flash.playFromStart();
                            }else if(progress.getProgress() == 1){
                                flash.stop();
                            }
                        }
                        progressText.setText(String.format("%.2f%%", progress.getProgress() * 100));
                    }
                }
        );
        
        needAnimation.addListener(
            new ChangeListener<Boolean>(){
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        if(newValue == false){
                            flash.stop();
                        }
                    }
            }
        );
    }    
    
}
