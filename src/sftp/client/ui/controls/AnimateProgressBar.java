/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui.controls;

import java.io.IOException;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 * @file AnimateProgressBar.java
 * @author Wensheng Yan
 * @date Apr 6, 2014
 */
public class AnimateProgressBar extends AnchorPane {
    AnimateProgressBarController controller;
    Node view;
    
    public AnimateProgressBar(){
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/sftp/client/ui/fxml/AnimateProgressBar.fxml"));
        fxmlLoader.setControllerFactory((Class<?> param)-> {
                return controller = new AnimateProgressBarController();
        });
        try {
            view = (Node) fxmlLoader.load();
        } catch (IOException ex) {
        }
        getChildren().add(view); 
        controller.progress.prefHeightProperty().bind(this.prefHeightProperty());
        controller.progress.prefWidthProperty().bind(this.prefWidthProperty());
    }
    
    public void setProgress(double num) {
        controller.progress.setProgress(num);
    }

    public double getProgress() {
        return controller.progress.getProgress();
    }
    
    public DoubleProperty progressProperty(){
        return controller.progress.progressProperty();
    }
    
    public void setMessage(String msg){
        controller.progressText.setText(msg);
    }
    
    public String getMessage(){
        return controller.progressText.getText();
    }
    
    public StringProperty MessageProperty(){
        return controller.progressText.textProperty();
    }
    
    public void setNeedAnimation(boolean flag){
        controller.needAnimation.set(flag);
    }
    
    public boolean getNeedAnimation(){
        return controller.needAnimation.get();
    }
    
    public BooleanProperty NeedAnimationProperty(){
        return controller.needAnimation;
    }
}
