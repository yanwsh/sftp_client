/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui.controls;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import sftp.client.Utility;
import sftp.client.ui.MainFrameController;
import sftp.lib.Console;
import sftp.lib.io.FileManage;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;

/**
 * @file FileListCell.java
 * @author Wensheng Yan
 * @date Apr 4, 2014
 */
public class FileListCell extends TreeTableRow<FileInfo> {
    public static DataFormat dataFormat = new DataFormat("filecell");
    public static DataFormat IdFormat = new DataFormat("Id");
    private Label dragView;
    private String parentId;
    
    public FileListCell(String parentId, Label dragView){
        this.parentId = parentId;
        this.dragView = dragView;
    }

    @Override
    public void updateItem(FileInfo item, boolean empty) {
        super.updateItem(item, empty);             
        setOnDragDetected(new EventHandler <MouseEvent>() {
            public void handle(MouseEvent event) {
                /* drag was detected, start drag-and-drop gesture*/

                /* allow any transfer mode */
                Dragboard db = startDragAndDrop(TransferMode.ANY);
                
                TreeItem<FileInfo> temp = getTreeItem();
                if(temp == null) return;

                FileInfo info = temp.getValue();
                if(info.IsParentDirectory()){
                    return;
                }
                /* put a string on dragboard */
                ClipboardContent content = new ClipboardContent();

                if(temp != null){
                    dragView.toFront();
                    dragView.setText(info.getFileName());
                    Image img = Utility.getFileIcon(this, info);
                    dragView.setGraphic(new ImageView(img));
                    dragView.setOpacity(0.5);
                    dragView.setMouseTransparent(true);
                    content.put(IdFormat, parentId);
                    content.put(dataFormat,info);             
                }else{
                    content.put(dataFormat,info);  
                    content.put(dataFormat,"null");       
                }
                db.setContent(content); 
                
                event.consume();
            }
        });
        
        setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent e) {
                dragView.setVisible(false);
                e.consume();
            }
        });
    }
}
