/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui.controls;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.paint.Color;
import sftp.client.ui.Entity.QueueFileInfo;

/**
 * @file TransferFileProgressCell.java
 * @author Wensheng Yan
 * @date Apr 11, 2014
 */
public class TransferFileProgressCell extends TableCell<QueueFileInfo, Double> {
       @Override
        public void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) 
            {
                setText(null);
                setGraphic(null);
            }
            else{
                AnimateProgressBar bar = new AnimateProgressBar();
                bar.setNeedAnimation(false);
                this.setText(null);
                bar.setProgress(item);
                bar.setPrefWidth(140);
                this.setGraphic(bar);
            }
        }
}
