/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui.controls;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import sftp.client.ui.MainFrameController;
import sftp.lib.io.FileManage;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;

/**
 * @file LocalFileListCell.java
 * @author Wensheng Yan
 * @date Apr 6, 2014
 */
public class LocalFileListCell extends FileListCell {
    private MainFrameController controller;
    
    public LocalFileListCell(String parentId, Label dragView, MainFrameController controller) {
        super(parentId, dragView);
        this.controller = controller;
    }

    @Override
    public void updateItem(FileInfo item, boolean empty) {
        super.updateItem(item, empty); 
        setOnMouseClicked(new EventHandler <MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(event.getButton().equals(MouseButton.PRIMARY)){
                    if(event.getClickCount() == 2){
                        Path path = null;
                        if(getItem() == null){
                            return;
                        }
                        if(getItem().equals(new FileInfo("..", true, 0, ""))){
                            path = controller.currentDir.getParent();
                        }else if(getItem().isDirectory()){
                            path = controller.currentDir.resolve(getItem().getFileName());
                        }
                        if(path != null){
                            controller.currentDir = path;
                            try {
                                DirectoryInfo info = FileManage.getDirectoryInfo(controller.currentDir.toAbsolutePath().toString());
                                TreeItem<FileInfo> currentRoot = getTreeItem().getParent();
                                currentRoot.getChildren().clear();
                                controller.updateFilesList(currentRoot, info, controller.currentDir.getParent() == null);
                            } catch (NoSuchFileException ex) {
                                Logger.getLogger(FileListCell.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        });
    }
}
