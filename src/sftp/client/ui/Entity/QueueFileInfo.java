/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui.Entity;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @file QueueFileInfo.java
 * @author Wensheng Yan
 * @date Apr 12, 2014
 */
public class QueueFileInfo {
    private StringProperty from;
    private StringProperty to;
    private StringProperty filename;
    private LongProperty size;
    private DoubleProperty progress;
    private StringProperty speed;

    public QueueFileInfo(String from, String to, String filename, long size) {
        this.from = new SimpleStringProperty(from);
        this.to = new SimpleStringProperty(to);
        this.filename = new SimpleStringProperty(filename);
        this.size = new SimpleLongProperty(size);
        this.progress = new SimpleDoubleProperty(0.0);
        this.speed = new SimpleStringProperty("");
    }

    public StringProperty fromProperty() {
        return from;
    }

    public StringProperty toProperty() {
        return to;
    }

    public StringProperty filenameProperty() {
        return filename;
    }

    public LongProperty sizeProperty() {
        return size;
    }

    public DoubleProperty progressProperty() {
        return progress;
    }

    public StringProperty speedProperty() {
        return speed;
    }   
}
