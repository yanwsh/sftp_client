/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.TreeItem;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Client;
import sftp.client.Main;
import sftp.lib.Console;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;

/**
 * @file FileUpdateThread.java
 * @author Wensheng Yan
 * @date Apr 5, 2014
 */
public class FileUpdateThread extends BaseThread {
    
    public FileUpdateThread(MainFrameController controller, Client client){
        super(controller, client);
    }
    @Override
    public void run() {
        super.run();

        if(!client.isAlive()){
            Platform.runLater(()->{
                Dialogs.create()
                       .message("Fail to connect to server!")
                       .showError();
                controller.remoteDirList.setRoot(null);
            });
            return;
        }
        
        while(client.getDirInfo() == null){
            updateMessage();
        }
        
        Platform.runLater(()->{
            DirectoryInfo dirInfo = client.getDirInfo();
            controller.remoteDir = dirInfo.getDirectory();
            boolean isRoot = false;
            if(controller.remoteDir.equals("/")){
                isRoot = true;
            }
            final TreeItem<FileInfo> root =  new TreeItem<>(new FileInfo());
            root.getChildren().clear();
            controller.remoteDirList.setRoot(root);
            controller.updateFilesList(root, dirInfo, isRoot);
            client.setDirInfo(null);
            while(!Console.messageList.isEmpty()){
                controller.logview.getItems().add(0, Console.messageList.poll());
            }
        });
    }
}
