/*
 * Copyright (C) 2014 wensheng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import sftp.client.Client;
import sftp.client.ProtocolState;
import sftp.lib.socket.Entity.UserLogin;

/**
 *
 * @author wensheng
 */
public class UserInfoChangeThread extends Thread {
    Client client;
    Stage stage;
    UserLogin info;
    
    public UserInfoChangeThread(Client client, Stage stage, UserLogin info){
        this.client = client;
        this.stage = stage;
        this.info = info;
    }
    
    @Override
    public void run() {
        client.updateUserInfo(info);
        
        synchronized(client){
            client.notify();
        }
        
        if(!client.isAlive()){
            Platform.runLater(()->{
                Dialogs.create()
                       .message("Fail to connect to server!")
                       .showError();
            });
            return;
        } 
        
        while(client.updatestate == ProtocolState.Unknown){
                    synchronized(client){
                        try {
                            client.wait();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(FileUpdateThread.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
        }
        
        if(client.loginstate == ProtocolState.Error){
                    Platform.runLater(()->{
                        Dialogs.create()
                        .message("Update Failed.")
                        .showError();
                    });
        }
        else{
            Platform.runLater(()->{
                stage.close();
            });
        }
        
    }
}
