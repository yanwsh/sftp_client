/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package sftp.client.ui;


import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.*;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.*;
import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.dialog.Dialogs;
import sftp.client.*;
import sftp.client.protocol.Login;
import sftp.lib.crypto.MD5;
import sftp.lib.socket.Entity.UserLogin;

/**
 * FXML Controller class
 *
 * @author wensheng
 */
public class LoginController extends BaseController {
    @FXML
            MediaView bannerPlayer;
    @FXML
            Text title;
    @FXML
            ImageView usr_icon;
    @FXML
            ImageView bannerImage;
    @FXML
            ComboBox usrIds;
    @FXML
            PasswordField usrPass;
    @FXML
            AnchorPane dragPane;
    @FXML
            AnchorPane mainPane;
    @FXML
            ProgressIndicator loadingprogress;
    @FXML
            AnchorPane loadingPane;
    @FXML
            CheckBox savePass;
    @FXML
            CheckBox autoLogin;
    
    private ObservableList<String> usrlists = FXCollections.observableArrayList();
    
    public BooleanProperty isShowing = new SimpleBooleanProperty(false);
    
    Client client;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        
        Rectangle rect = new Rectangle(380,300);
        rect.setStyle("MainClip");
        rect.setArcHeight(20.0);
        rect.setArcWidth(20.0);
        mainPane.setClip(rect);
        
        //MediaPlayer player = getAudioMediaPlayer();
        //weatherPlayer.setMediaPlayer(player);
        //player.play();
        //player.setMute(true);
        //player.setCycleCount(Timeline.INDEFINITE);
        Image banner = new Image(Utility.getFileFromResource(this, "images/weather.jpg"));
        bannerImage.setImage(banner);
        
        //create complex linear gradient
        LinearGradient gradient2 = new LinearGradient(0, 0, 0, 0.5,  true, CycleMethod.REFLECT, new Stop[] {
            new Stop(0, Color.DODGERBLUE),
            new Stop(0.1, Color.BLACK),
            new Stop(1, Color.DODGERBLUE)
        });
        
        //set rectangle fill
        title.setFill(gradient2);
        
        Rectangle usr_icon_rect = new Rectangle(80,80);
        usr_icon_rect.setArcHeight(8.0);
        usr_icon_rect.setArcWidth(8.0);
        usr_icon.setClip(usr_icon_rect);
        
        usrIds.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String changedUser) {
                if(t == null){
                    return;
                }
                Platform.runLater(() -> {
                    usrIds.setValue(changedUser);
                    ArrayList<UserLogin> users = application.conf.getSavedUsers();
                    Object[] objs = users.stream().filter((UserLogin user) -> {
                        return user.getUsername().equals(changedUser);
                    }).toArray();
                    Image icon = null;
                    if(objs.length > 0){
                        UserLogin user = (UserLogin)objs[0];
                        if(!user.getPassword().equals("")){
                            usrPass.setText("******");
                            savePass.selectedProperty().set(true);
                        }else{
                            usrPass.setText("");
                            savePass.selectedProperty().set(false);
                        }
                        if(application.conf.getAutoUsername().equals(changedUser)){
                            autoLogin.selectedProperty().set(true);
                        }
                        String strIcon = user.getIcon();
                        if(!strIcon.equals(""))
                            icon = Utility.convertToFXImage(sftp.lib.Utility.parseBase64Binary(strIcon));
                    }
                    
                    if(icon == null){
                        icon = new Image(Utility.getFileFromResource(this, "images/default-icon.jpg"));
                    }
                    usr_icon.setImage(icon);
                });
            }
        });
        
        Platform.runLater(() -> {
            ArrayList<UserLogin> users = application.conf.getSavedUsers();
            UserLogin defaultUser = null;
            users.stream().forEach((UserLogin user) -> {
                usrlists.add(user.getUsername());
            });
            usrIds.setItems(usrlists);
            usrIds.setEditable(true);
            
            Object[] objs = users.stream().filter((UserLogin user) -> {
                return user.getUsername().equals(application.conf.getLastUsername());
            }).toArray();
            if(objs.length > 0){
                defaultUser = (UserLogin)objs[0];
            }else{
                if(users.size() > 0){
                    defaultUser = users.get(0);
                }
            }
            
            if(defaultUser != null){
                usrIds.setValue(defaultUser.getUsername());
                if(!defaultUser.getPassword().equals("")){
                    usrPass.setText("******");
                    savePass.selectedProperty().set(true);
                }
            }
            if(!application.conf.getAutoUsername().equals("")){
                autoLogin.selectedProperty().set(true);
            }
            Image icon = null;
            if(defaultUser != null){
                String strIcon = defaultUser.getIcon();
                if(!strIcon.equals(""))
                    icon = Utility.convertToFXImage(sftp.lib.Utility.parseBase64Binary(strIcon));
            }
            
            if(icon == null){
                icon = new Image(Utility.getFileFromResource(this, "images/default-icon.jpg"));
            }
            usr_icon.setImage(icon);
            
            loadingPane.visibleProperty().bind(isShowing);
            loadingprogress.visibleProperty().bind(isShowing);
        });
    }
    
    public void onBtnLoginClicked(MouseEvent event){
        String name = (String)usrIds.getValue();
        String usrpass = usrPass.getText();
        if(name == null || name.equals("")){
            Platform.runLater(()->{
                Dialogs.create()
                        .message("Please type your user name!")
                        .showError();
            });
            return;
        }
        if(usrpass == null || usrpass.equals("")){
            Platform.runLater(()->{
                Dialogs.create()
                        .message("Please type your password!")
                        .showError();
            });
            return;
        }
        UserLogin loginUser = null;
        Object[] objs = application.conf.getSavedUsers().stream().filter((UserLogin user) -> {
            return user.getUsername().equals(name);
        }).toArray();
        if(objs.length > 0){
            loginUser = (UserLogin)objs[0];
        }
        String pass = "";
        if(loginUser != null && !loginUser.getPassword().equals("") && usrpass.equals("******")){
            pass = loginUser.getPassword();
        }else{
            MD5 md5 = new MD5();
            pass = md5.EncryptToString(usrpass);
        }
        Platform.runLater(()->{
            isShowing.set(true);
        });
        
        new LoginThread(application, this, name, pass).start();
    }
    
}
