/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client;

import java.io.*;
import java.net.*;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import sftp.client.protocol.*;
import sftp.client.protocol.IProtocol;
import sftp.lib.Console;
import sftp.lib.Utility;
import sftp.lib.crypto.AES;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.crypto.RSA;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.UserLogin;
import sftp.lib.socket.InvalidPackageException;
import sftp.lib.socket.SignType;
import sftp.lib.socket.SocketHelper;
import sftp.lib.socket.TransferPackage;

/**
 * @file Client.java
 * @author Wensheng Yan
 * @date Mar 24, 2014
 */
public class Client extends Thread {
    private static final int TRYREADTIMES = 3;
    private Config conf;
    private Socket sock;
    public PublicKey sevPublicKey;
    private boolean isFinish;
    private PriorityQueue<IProtocol> events;
    private DirectoryInfo dirinfo;
    private PublicKey clientPublicKey;
    private PrivateKey clientPrivateKey;
    private byte[] sessionKey;
    public enum ClientType { Main, Assistant};
    private ClientType clienttype;
    public ProtocolState loginstate;
    public ProtocolState updatestate;
    public UserLogin userInfo;
    private String ErrorMessage;
    
    public Client(Config conf){
        this.conf = conf;
        this.loginstate = ProtocolState.Unknown;
        this.userInfo = null;
        try {
            Serializable s = ObjectHandler.FromBytes(Utility.parseBase64Binary(conf.getKey()));
            if(s instanceof KeyPair){
		KeyPair key = (KeyPair)s;
                clientPublicKey= key.getPublic();
                clientPrivateKey = key.getPrivate();
            }else{
		throw new Exception("Error: can not analysis client key.");
            }
            sock = new Socket(conf.getServerUrl(), conf.getServerPort());
            events = new PriorityQueue<IProtocol>(10,
                    (IProtocol protocol1, IProtocol protocol2) -> {
                        if(protocol1 == null || protocol2 == null){
                            return 0;
                        }
                        return Integer.valueOf(protocol1.getPriority()).compareTo(protocol2.getPriority());
                    }
            );
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.ALL, null, ex);
            sock = null;
            Console.println("fail to connect the server.");
        } catch (Exception ex){
            Logger.getLogger(Client.class.getName()).log(Level.ALL, null, ex);
            sock = null;
            Console.println(ex.getMessage());
        }
    }
    
    public Client(Config conf, ClientType type){
        this(conf);
        this.clienttype = type;
        if(type == ClientType.Main && sock != null){
            initEvent();
        }
    }
    
    /**
     * fork to a new sub client
     * @param c 
     */
    public void fork(Client c){
        this.sessionKey = c.sessionKey;
        this.sevPublicKey = c.sevPublicKey;
        this.userInfo = new UserLogin(c.userInfo);
    }
    
    @Override
    public void start(){
        if(sock != null){
            super.start();
        }
    }
    
    @Override
    public void run(){
        if(sock == null){
           Console.println("Error: Sock is null.");     
           return;
        }  
        OutputStream writer = null;
        byte[] recData; 
        TransferPackage tp;
        try {
            writer = sock.getOutputStream();
            InputStream reader = sock.getInputStream();
            SocketHelper helper = new SocketHelper(reader);
            isFinish = false;
            while(!isFinish){
                synchronized(this){
                    while(events.isEmpty()){
                        try {
                            this.wait();
                            if(isFinish) break;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                if(isFinish) break;
                IProtocol pol = events.poll();
                byte[] polBytes = pol.sender();
                if(polBytes == null){continue;}
                tp = new TransferPackage(pol.getDataType(), pol.isIsEncrypt(), pol.getEntype(), polBytes);
                
                if(tp.IsEncrypt()){
                    encryptPackage(tp);
                }
                SignPackage(pol.getSigntype(), tp);
                byte[] sendBytes = ObjectHandler.ToBytes(tp);
                
                try{
                    writer.write(sendBytes);
                }catch(SocketException e){
                    Console.println("Error: Connection abort.");
                    break;
                }
                
                if(!pol.isNeedRec()) continue;
                try{
                    recData = helper.readSocket();
                }catch(SocketException e){
                    Console.println("Error: Connection abort.");
                    break;
                }
                
                Serializable s = ObjectHandler.FromBytes(recData);
				
                if(!(s instanceof TransferPackage)){
                    int readTimes = TRYREADTIMES;
                    int availableData = 0;

                    while(readTimes > 0){
                        try {
                            sleep(500);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        availableData = reader.available();
                        if(availableData == 0){
                            readTimes--;
                        }else{
                            byte[] RecTmpData = helper.readSocket();
                            byte[] tmp = new byte[RecTmpData.length + recData.length];
                            System.arraycopy(recData, 0, tmp, 0, recData.length);
                            System.arraycopy(RecTmpData, 0, tmp, recData.length, RecTmpData.length);
                            recData = tmp;
                            s = ObjectHandler.FromBytes(recData);
                            if(s == null){
                                readTimes--;
                            }else{
                                break;
                            }
                        }
                    }
                    
                    if(s == null){
                        Console.println("Error: Read package error, send again.");
                        pol.resetAndRetry();
                        continue;
                    }
                }
            
                tp = (TransferPackage)s;
                boolean flag = checkSignature(tp);
                if(!flag){
                    Console.println("Error: Package signature error, send again.");
                    pol.resetAndRetry();
                    continue;
                }
                if(tp.IsEncrypt()){
                    decryptPackage(tp);
                }
                try{
                    pol.receiver(tp.getData());
                }
                catch(InvalidPackageException e){
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, e);
                }
                synchronized(this){
                    this.notify();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void encryptPackage(TransferPackage tp){
	EncryptType type = tp.getEntype();
	if(type == EncryptType.AES){
            AES aes = new AES();
            tp.setData(aes.EncryptBytes(sessionKey, tp.getData()));
	}else if(type == EncryptType.RSA){
            RSA rsa = new RSA();
            tp.setData(rsa.Encrypt(sevPublicKey, tp.getData()));
	}else if(type == EncryptType.ShareKey){
            AES aes = new AES();
            tp.setData(aes.EncryptBytes(ObjectHandler.getSharedkey(), tp.getData()));
        }
    }
	
    private void decryptPackage(TransferPackage tp){
        EncryptType type = tp.getEntype();
	if(type == EncryptType.AES){
            AES aes = new AES();
            tp.setData(aes.DecryptBytes(sessionKey, tp.getData()));
	}
	else if(type == EncryptType.RSA){
            RSA rsa = new RSA();
            tp.setData(rsa.DecryptBytes(clientPrivateKey, tp.getData()));
	}else if(type == EncryptType.ShareKey){
            AES aes = new AES();
            tp.setData(aes.DecryptBytes(ObjectHandler.getSharedkey(), tp.getData()));
        }
    }
    
    private void SignPackage(SignType type, TransferPackage tp){
        tp.setSignType(type);
        if(type == SignType.ShareKey){
            tp.setSign(ObjectHandler.signDataByAES(tp.getData()));
        }else if(type == SignType.PrivateKey){
            tp.setSign(ObjectHandler.signData(clientPrivateKey, tp.getData()));
        }
    }
    
    private boolean checkSignature(TransferPackage tp){
        boolean flag = false;
        SignType signtype = tp.getSignType();
        if(signtype == SignType.ShareKey){
            flag = ObjectHandler.checkSignByAES(tp.getData(), tp.getSign());
        }else if(signtype == SignType.PrivateKey){
            flag = ObjectHandler.checkSign(sevPublicKey, tp.getData(), tp.getSign());
        }
        return flag;
    }

    public boolean isIsFinish() {
        return isFinish;
    }

    public void setIsFinish(boolean isFinish) {
        this.isFinish = isFinish;
    }

    public Config getConf() {
        return conf;
    }

    public void setConf(Config conf) {
        this.conf = conf;
    }

    public PriorityQueue<IProtocol> getEvents() {
        return events;
    }

    public void setEvents(PriorityQueue<IProtocol> e) {
        events = e;
    }

    private void initEvent() {
        events.add(new GetServerPulbicKey(this));
        //events.add(new checkClientPublicKey(this));
        //events.add(new Login(this));
        //events.add(new GetSessionKey(this));
        //events.add(new getDirectoryAndFileList(this));
    }
    
        
    public void registerThread(){
        String password = userInfo.getPassword();
        AES aes = new AES();
        byte[] e = aes.EncryptBytes(sessionKey, password.getBytes());
        password = sftp.lib.Utility.printBase64Binary(e);
        UserLogin login = new UserLogin(userInfo.getUsername(), password);
        events.add(new registerSubThread(this, login));
    }
    
    public void keepAlive(){
        events.add(new keepAlive(this));
    }
    
    public void GetUserDiskInfo(UserLogin info){
        events.add(new GetDiskInfo(this, info));
    }
    
    public void DeleteFile(DirectoryInfo info){
        events.add(new DeleteFile(this, info));
    }
    
    public void updateUserInfo(UserLogin info){
        events.add(new ChangeUserInfo(this, info));
    }
    
    public void getDir(String Directory){
        events.add(new getDirectoryAndFileList(this, Directory, false));
    }
    
    public void getDir(String Directory, boolean afterSend){
        events.add(new getDirectoryAndFileList(this, Directory, true));
    }
    
    public void getFile(String filename, String clientdir, String serverdir){
        events.add(new GetFile(this, filename, clientdir, serverdir));
    }
    
    public void putFile(String filename, String serverdir, String clientdir){
        events.add(new SendFile(this, filename, clientdir, serverdir));
    }
    
    public void close(){
        events.add(new Close(this));
    }

    public PublicKey getClientPublicKey() {
        return clientPublicKey;
    }

    public void setSessionKey(byte[] sessionKey) {
        this.sessionKey = sessionKey;
    }

    public byte[] getSessionKey() {
        return sessionKey;
    }

    public DirectoryInfo getDirInfo() {
        return dirinfo;
    }

    public void setDirInfo(DirectoryInfo dirinfo) {
        this.dirinfo = dirinfo;
    }

    public ClientType getClienttype() {
        return clienttype;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }
    
    
    
}
