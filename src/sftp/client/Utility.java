/*
* Copyright (C) 2014 Wensheng Yan
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package sftp.client;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import sftp.client.ui.SettingController;
import sftp.lib.socket.Entity.FileInfo;


/**
 * @file Utility.java
 * @author Wensheng Yan
 * @date Mar 30, 2014
 */
public class Utility {
    private static final long K = 1024;
    private static final long M = K * K;
    private static final long G = M * K;
    private static final long T = G * K;
    
    public static Image getFileIcon(Object obj, FileInfo file){
        Image fileicon = null;
        if(file.isDirectory()){
            fileicon = new Image(Utility.getFileFromResource(obj, "/sftp/client/ui/images/folder.png"), 20, 20, false, true);
        }else{
            String ext = Utility.getFileExtension(file.getFileName());
            if(ext != ""){
                ext = ext + ".png";
                InputStream in = Utility.getFileFromResource(obj, "/sftp/client/ui/images/icon/" + ext);
                if(in != null){
                    fileicon = new Image(in, 20, 20, false, true);
                }
            }
        }
        if(fileicon == null){
            fileicon = new Image(Utility.getFileFromResource(obj, "/sftp/client/ui/images/default-icon.png"), 20, 20, false, true);
        }
        return fileicon;
    }
    
    public static InputStream getFileFromResource(Object obj, String name){
        InputStream inputStream = null;
        inputStream = obj.getClass().getResourceAsStream(name);
        return inputStream;
    }
    
    public static String getFileName(String file){
        String name = "";
        int pos = file.lastIndexOf(".");
        if(pos != -1){
            name = file.substring(0, pos);
        }
        return name;
    }
    
    public static String getFileExtension(String file){
        String ext = "";
        int pos = file.lastIndexOf(".");
        if(pos != -1){
            ext = file.substring(pos + 1);
        }
        return ext;
    }
    
    public static InputStream getFile(String name){
        InputStream inputStream = null;
        try{
            inputStream = new FileInputStream(name);
        }
        catch(FileNotFoundException ex){
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inputStream;
    }
    
    public static String getFileSize(long size){
        final long[] dividers = new long[] { T, G, M, K, 1 };
        final String[] units = new String[] { "TB", "GB", "MB", "KB", "B" };
        if(size == 0)
            return "0B";
        if(size < 0)
            throw new IllegalArgumentException("Invalid file size: " + size);
        String result = null;
        for(int i = 0; i < dividers.length; i++){
            final long divider = dividers[i];
            if(size >= divider){
                double tmp = divider > 1 ? (double) size / (double) divider : (double) size;
                result = String.format("%.1f %s", tmp, units[i]);
                break;
            }
        }
        return result;
    }
    
    public static javafx.scene.image.Image ConvertToGrayImage(javafx.scene.image.Image fxImage){
        BufferedImage image = new BufferedImage((int)fxImage.getWidth(), (int)fxImage.getHeight(),
                BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = image.getGraphics();
        java.awt.image.BufferedImage awtImg = Utility.convertToAwtImage(fxImage);
        g.drawImage(awtImg, 0, 0, null);
        g.dispose();
        
        try{
            fxImage = Utility.createImage(image);
        }
        catch(Exception e){
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, e);
        }
        return fxImage;
    }
    
    public static byte[] ConvertImageToByteArray(javafx.scene.image.Image image, String extension) throws IOException{
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BufferedImage bufferImage = SwingFXUtils.fromFXImage(image, null);
        if(!extension.equals("gif") && !extension.equals("jpg") && !extension.equals("png")){
            extension = "png";
        }
        
        ImageIO.write(bufferImage, extension, out);

        return out.toByteArray();
    }
    
    
    public static javafx.scene.image.Image createImage(java.awt.Image image) throws IOException {
        if (!(image instanceof RenderedImage)) {
            BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
                    image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
            Graphics g = bufferedImage.createGraphics();
            g.drawImage(image, 0, 0, null);
            g.dispose();
            
            image = bufferedImage;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write((RenderedImage) image, "png", out);
        out.flush();
        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        return new javafx.scene.image.Image(in);
    }
    
    public static java.awt.image.BufferedImage convertToAwtImage(javafx.scene.image.Image fxImage) {
        java.awt.image.BufferedImage awtImage = new BufferedImage((int)fxImage.getWidth(), (int)fxImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
        SwingFXUtils.fromFXImage(fxImage, awtImage);
        return (BufferedImage)awtImage;
    }
    
    public static BufferedImage convertToBufferedImage(byte[] b) {
        BufferedImage image = null;
        try{
            ByteArrayInputStream bais = new ByteArrayInputStream(b);
            image = ImageIO.read(bais);
        }
        catch(IOException e){
        }
        return image;
    }
    
    public static Image convertToFXImage(byte[] b){
        BufferedImage buffImg = convertToBufferedImage(b);
        return SwingFXUtils.toFXImage(buffImg, null);
    }
}
