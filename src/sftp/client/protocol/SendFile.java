/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import sftp.client.Client;
import sftp.client.FileTransferManageCenter;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.io.FileManage;
import sftp.lib.socket.DataPackage;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.FileHandleType;
import sftp.lib.socket.InvalidPackageException;
import sftp.lib.socket.SignType;

/**
 * @file SendFile.java
 * @author Wensheng Yan
 * @date Apr 12, 2014
 */
public class SendFile extends IProtocol {
    private int MAXTransfer;
    DataPackage datapackage;
    FileInputStream fis;
    byte[] Data;
    long preIndex;
    
    public SendFile(Client client, String filename, String clientDir, String servSaveDir){
        super(client, DataType.data, true, EncryptType.AES, Priority.sendFile.getValue(), SignType.PrivateKey);
        this.MAXTransfer = client.getConf().getMaxPkgSize();
        String source = clientDir + File.separator + filename;
        fis = null;
        Data = new byte[MAXTransfer];
        long pkgsize = 0;
        long filesize = 0;
        if(!FileManage.checkFile(source)){
            FileTransferManageCenter.updateMSG(filename, "Err: File does not exist.");
            Console.println("Err: File does not exist.");
            return;
        }
        try {
            fis = new FileInputStream(source);
            File file = new File(source);
            long size = file.length();
            filesize = size;
            pkgsize = size / MAXTransfer;
            if(size % MAXTransfer != 0){
		pkgsize++;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendFile.class.getName()).log(Level.SEVERE, null, ex);
            fis = null;
        }
        this.datapackage = new DataPackage(FileHandleType.put, filename, servSaveDir, pkgsize);
        this.datapackage.setFilesize(filesize);
    }

    @Override
    public byte[] sender() {
        DataPackage dpg = null;
        if(fis == null){
            FileTransferManageCenter.updateMSG(datapackage.getFileName(), "Error: fis could not be null.");
            Console.println("Error: fis could not be null. File Name: " + datapackage.getFileName());
            return null;
        }
        else
        {
            if(datapackage.getCurrentPackage() == preIndex){
		datapackage.setData(Data);
            }
            else{
                try {
                    //resize data array if it is too small
                    if(Data.length < MAXTransfer){
                        Data = new byte[MAXTransfer];
                    }
                    int retSize = fis.read(Data, 0, MAXTransfer);
                    if(retSize >0 && retSize != MAXTransfer){
                        byte[] tmp = new byte[retSize];
                        System.arraycopy(Data, 0, tmp, 0, retSize);
                        Data = tmp;
                    }
                    if((retSize != MAXTransfer) && (datapackage.getCurrentPackage() != datapackage.getTotalPackage()) || (retSize < 0)){
                        FileTransferManageCenter.updateMSG(datapackage.getFileName(), "Error: cannot read to the end.");
                        Console.println("Error: cannot read to the end. File Name: " + datapackage.getFileName());
                        return null;
                    }else{
                        dpg = datapackage;
                        dpg.setData(Data);
                        preIndex = dpg.getCurrentPackage();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ObjectHandler.ToBytes(dpg);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof DataPackage)){
            throw new InvalidPackageException();
        }
        if(s instanceof DataPackage){
            DataPackage dpg = (DataPackage)s;
            if(dpg.getHandleType() == FileHandleType.error){
                Console.println(dpg.getParam());
                FileTransferManageCenter.updateMSG(dpg.getFileName(), (String)dpg.getParam());
                return;
            }
            long total = dpg.getTotalPackage();
            if(dpg.getHandleType() == FileHandleType.put){
                //update current package
                long current = dpg.getCurrentPackage();
                double percentage = current / (double)total;
                FileTransferManageCenter.update(dpg.getFileName(), Data.length, percentage, current);
                datapackage.setCurrentPackage(current);
                client.getEvents().add(this);
            }
            if(dpg.getHandleType() == FileHandleType.success){
                FileTransferManageCenter.update((String)dpg.getFileName(), Data.length, 1.0, total);
                //file transfer finish
                try {
                    fis.close();
                } catch (IOException e) {
                    Logger.getLogger(SendFile.class.getName()).log(Level.SEVERE, null, e);
                }
                fis = null;
            }
        }
    }
}
