/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.Serializable;
import java.security.PublicKey;
import java.util.Arrays;
import sftp.client.Client;
import sftp.client.Config;
import sftp.client.Main;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.crypto.SHA256;
import sftp.lib.socket.CommandPackage;
import sftp.lib.socket.CommandType;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.InvalidPackageException;

/**
 * @file GetServerPulbicKey.java
 * @author Wensheng Yan
 * @date Mar 26, 2014
 */
public class GetServerPulbicKey extends IProtocol {
    
    public GetServerPulbicKey(Client client){
        super(client, DataType.command, false, EncryptType.None, Priority.ServerPublicKey.getValue());
    }

    @Override
    public byte[] sender() {
        CommandPackage cp = new CommandPackage(CommandType.getServerPublicKey);
        return ObjectHandler.ToBytes(cp);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof CommandPackage)){
            throw new InvalidPackageException();
        }
        
        CommandPackage cp = (CommandPackage)s;
        Serializable sp = cp.getParam();
        if(!((sp instanceof PublicKey))){
            throw new InvalidPackageException();
        }
        
        Config conf = client.getConf();
        byte[] hash = sftp.lib.Utility.parseBase64Binary(conf.getServPublicHash());
        SHA256 sha = new SHA256();
        byte[] test = sha.Encrypt(sp);
        if(!Arrays.equals(hash, test)){
            ConnectionTerminated("Error: hash value match failed. Client will be closed.");
        }
        
        client.sevPublicKey =  (PublicKey)sp;
    }

}
