/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import sftp.client.Client;
import sftp.client.Main;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.socket.CommandPackage;
import sftp.lib.socket.CommandType;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.InvalidPackageException;
import sftp.lib.socket.SignType;

/**
 * @file Close.java
 * @author Wensheng Yan
 * @date Mar 27, 2014
 */
public class Close extends IProtocol {
    
    public Close(Client client){
        super(client, DataType.command, false, EncryptType.None, Priority.close.getValue(), false, SignType.ShareKey);
    }

    @Override
    public byte[] sender() {
        CommandPackage cp = new CommandPackage(CommandType.close);
        Console.println("MSG: client close.");
        client.setIsFinish(true);
        return ObjectHandler.ToBytes(cp);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
    }

}
