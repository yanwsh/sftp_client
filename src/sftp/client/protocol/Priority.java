/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

/**
 *
 * @author Wensheng Yan
 */
public enum Priority {
    ServerPublicKey(1),
    Login(1),
    registerSubThread(1),
    ChangeUserInfo(1),
    getDiskInfo(1),
    checkClientPublicKey(2),
    updateClientPublicKey(2),
    DeleteFile(5),
    getSessionKey(10),
    keepAlive(50),
    getDirectoryAndFile(18),
    getDirectoryAndFileAfterSend(25),
    getFile(21),
    sendFile(20),
    close(0);
    
    private int value;
    private Priority(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
