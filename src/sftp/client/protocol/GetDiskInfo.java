/*
 * Copyright (C) 2014 wensheng
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.Serializable;
import sftp.client.Client;
import sftp.client.ProtocolState;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.socket.CommandPackage;
import sftp.lib.socket.CommandType;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.Entity.UserLogin;
import sftp.lib.socket.InvalidPackageException;
import sftp.lib.socket.SignType;

/**
 *
 * @author wensheng
 */
public class GetDiskInfo extends IProtocol{
    private UserLogin userinfo;
    
    public GetDiskInfo(Client client, UserLogin userinfo){
        super(client, DataType.command, true, EncryptType.AES, Priority.getDiskInfo.getValue(), SignType.PrivateKey);
        this.userinfo = userinfo;
        client.updatestate = ProtocolState.Unknown;
    }
    
    @Override
    public byte[] sender() {
        CommandPackage cmdpkg = new CommandPackage(CommandType.getDiskInfo, userinfo);
        return ObjectHandler.ToBytes(cmdpkg);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof CommandPackage)){
            throw new InvalidPackageException();
        }
        
        CommandPackage cp = (CommandPackage)s;
        if(cp.getCmdType() == CommandType.success){
            client.updatestate = ProtocolState.Success;
            UserLogin info = (UserLogin)cp.getParam();
            client.userInfo.setDiskUsage(info.getDiskUsage());
            client.userInfo.setDiskCapacity(info.getDiskCapacity());
            
            Console.println("MSG: Get user info successfully.");
            
        } else if (cp.getCmdType() == CommandType.error){
            client.updatestate = ProtocolState.Error;
            Console.println(cp.getParam());
        }
    }
    
}
