/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import sftp.client.Client;
import sftp.client.FileTransferManageCenter;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.io.FileManage;
import sftp.lib.socket.DataPackage;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.FileHandleType;
import sftp.lib.socket.InvalidPackageException;
import sftp.lib.socket.SignType;

/**
 * @file GetFile.java
 * @author Wensheng Yan
 * @date Apr 5, 2014
 */
public class GetFile extends IProtocol {
    Path clientSaveDir;
    DataPackage datapackage;
    byte[] tmpData;
    
    public GetFile(Client client, String filename, String clientSaveDir, String servDir){
        super(client, DataType.data, true, EncryptType.AES, Priority.getFile.getValue(), SignType.PrivateKey);
        this.datapackage = new DataPackage(FileHandleType.get, filename, servDir, 0);
        this.clientSaveDir = Paths.get(clientSaveDir);
        this.tmpData = null;
    }

    @Override
    public byte[] sender() {
        DataPackage dp = datapackage;
        return ObjectHandler.ToBytes(dp);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof DataPackage)){
            throw new InvalidPackageException();
        }
        if(s instanceof DataPackage){
            DataPackage dpg = (DataPackage)s;
            if(dpg.getHandleType() == FileHandleType.error){
                Console.println(dpg.getParam());
                FileTransferManageCenter.updateMSG(dpg.getFileName(), (String)dpg.getParam());
                return;
            }
            this.datapackage = dpg;
            long current = dpg.getCurrentPackage();
            long total = dpg.getTotalPackage();
            
            double percentage = current / (double)total;
            FileTransferManageCenter.update(dpg.getFileName(), dpg.getData().length, percentage, current);

            if(tmpData == null){
                tmpData = this.datapackage.getData();
            }else{
                byte[] tmp = new byte[tmpData.length + this.datapackage.getData().length];
                System.arraycopy(tmpData, 0, tmp, 0, tmpData.length);
                System.arraycopy(this.datapackage.getData(), 0, tmp, tmpData.length, this.datapackage.getData().length);
                tmpData = tmp;
            }
            //save to temp file every 50M
            if(this.tmpData.length > 50000000 && (current != total)){
                try {
                    //save to disk
                    String tmpfilename = clientSaveDir + File.separator +  dpg.getFileName() + ".tmp";
                    if(!FileManage.checkFile(tmpfilename)){
                        FileManage.createFile(tmpfilename);
                    }   
                    FileOutputStream fos = new FileOutputStream(tmpfilename, true);
                    fos.write(this.tmpData);
                    fos.close();
                    this.tmpData = null;
                } catch (IOException ex) {
                    Logger.getLogger(GetFile.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            this.datapackage.setData(null);
            datapackage.setCurrentPackage(datapackage.getCurrentPackage() + 1);
            if(current < total){
                client.getEvents().add(this);
            }else if(current == total){
                try {
                    String savesource = clientSaveDir + File.separator +  dpg.getFileName();
                    FileManage.deleteFileIfExists(savesource);
                    String tmpfilename = savesource + ".tmp";
                    if(FileManage.checkFile(tmpfilename)){
                        FileManage.renameFile(tmpfilename, savesource);
                    }   
                    FileOutputStream fos = new FileOutputStream(savesource, true);
                    fos.write(this.tmpData);
                    fos.close();
                    Console.println(String.format("MSG: receive file %s successfully. Save it to %s", dpg.getFileName(), clientSaveDir));
                } catch (IOException ex) {
                    Logger.getLogger(GetFile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
