/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.Serializable;
import sftp.client.Client;
import sftp.client.ProtocolState;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.socket.CommandPackage;
import sftp.lib.socket.CommandType;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.Entity.UserLogin;
import sftp.lib.socket.InvalidPackageException;

/**
 * @file registerSubThread.java
 * @author Wensheng Yan
 * @date Apr 28, 2014
 */
public class registerSubThread extends IProtocol {
    private UserLogin login;
    
    public registerSubThread(Client client, UserLogin login){
        super(client, DataType.command, true, EncryptType.RSA, Priority.registerSubThread.getValue());
        this.login = login;
    }

    @Override
    public byte[] sender() {
        CommandPackage cmdpkg = new CommandPackage(CommandType.registerSubThread, login);
        return ObjectHandler.ToBytes(cmdpkg);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof CommandPackage)){
            throw new InvalidPackageException();
        }
        
        CommandPackage cp = (CommandPackage)s;
        if(cp.getCmdType() == CommandType.success){
            Console.println("MSG: register thread success.");
        } else if (cp.getCmdType() == CommandType.error){
            Console.println(cp.getParam());
            retry();
        }
    }

}
