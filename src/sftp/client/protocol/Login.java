/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.Serializable;
import sftp.client.Client;
import sftp.client.ProtocolState;
import sftp.client.Main;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.socket.CommandPackage;
import sftp.lib.socket.CommandType;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.Entity.UserLogin;
import sftp.lib.socket.InvalidPackageException;

/**
 * @file Login.java
 * @author Wensheng Yan
 * @date Mar 28, 2014
 */
public class Login extends IProtocol {
    private String username;
    private String password;
    public Login(Client client, String username, String password){
        super(client, DataType.command, true, EncryptType.RSA, Priority.Login.getValue());
        this.username = username;
        this.password = password;
        client.loginstate = ProtocolState.Unknown;
    }

    @Override
    public byte[] sender() {
        CommandPackage cmdpkg = new CommandPackage(CommandType.login, new UserLogin(username, password));
        return ObjectHandler.ToBytes(cmdpkg);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof CommandPackage)){
            throw new InvalidPackageException();
        }
        
        CommandPackage cp = (CommandPackage)s;
        if(cp.getCmdType() == CommandType.success){
            Console.println("MSG: login success.");
            client.loginstate = ProtocolState.Success;
            client.userInfo = (UserLogin)cp.getParam();
        } else if (cp.getCmdType() == CommandType.error){
            Console.println(cp.getParam());
            client.setErrorMessage((String)cp.getParam());
            client.loginstate = ProtocolState.Error;
        }
    }

}
