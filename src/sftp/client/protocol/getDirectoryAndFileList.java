/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import java.io.Serializable;
import java.util.Collection;
import java.util.PriorityQueue;
import sftp.client.Client;
import sftp.client.Main;
import sftp.lib.Console;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.io.FileManage;
import sftp.lib.socket.CommandPackage;
import sftp.lib.socket.CommandType;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.Entity.DirectoryInfo;
import sftp.lib.socket.Entity.FileInfo;
import sftp.lib.socket.InvalidPackageException;

/**
 * @file getDirectoryAndFileList.java
 * @author Wensheng Yan
 * @date Mar 29, 2014
 */
public class getDirectoryAndFileList extends IProtocol {
    private String directory;

    public getDirectoryAndFileList(Client client){
        this(client, "/", false);
    }
    
    public getDirectoryAndFileList(Client client, String directory, boolean afterSend) {
        super(client, DataType.command, true, EncryptType.AES, Priority.getDirectoryAndFile.getValue());
        if(afterSend){
            this.setPriority(Priority.getDirectoryAndFileAfterSend.getValue());
        }
        this.directory = directory;
    }

    @Override
    public byte[] sender() {
        CommandPackage cp = new CommandPackage(CommandType.getDirectoryAndFileList, new DirectoryInfo(this.directory));
        return ObjectHandler.ToBytes(cp);
    }

    @Override
    public void receiver(byte[] data) throws InvalidPackageException {
        Serializable s = ObjectHandler.FromBytes(data);
        if(!(s instanceof CommandPackage)){
            throw new InvalidPackageException();
        }
        
        CommandPackage cp = (CommandPackage)s;
        if(cp.getCmdType() == CommandType.success){
            Serializable param = cp.getParam();
            if(!(param instanceof DirectoryInfo)){
                throw new InvalidPackageException();
            }
            DirectoryInfo dirInfo = (DirectoryInfo)param;
            client.setDirInfo(dirInfo);

            Console.println("MSG: receive file directory successfully.");
                    
        } else if (cp.getCmdType() == CommandType.error){
            Console.println(cp.getParam());
            retry();
        }
    }

}
