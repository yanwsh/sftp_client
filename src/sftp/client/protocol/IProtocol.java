/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client.protocol;

import sftp.client.Client;
import sftp.client.Main;
import sftp.lib.Console;
import sftp.lib.socket.DataType;
import sftp.lib.socket.EncryptType;
import sftp.lib.socket.InvalidPackageException;
import sftp.lib.socket.SignType;

/**
 *
 * @author Wensheng Yan
 */
public abstract class IProtocol {
    private static final int TryTimes = 1;
    protected Client client;
    private DataType dataType;
    private boolean IsEncrypt;
    private EncryptType entype;
    private int priority;
    private boolean needRec;
    private SignType signtype;
    protected int ErrorTimes;
    
    public IProtocol(Client client, DataType dataType, boolean IsEncrypt, EncryptType entype, int priority) {
        this(client, dataType, IsEncrypt, entype, priority, true, SignType.ShareKey);
    }

    public IProtocol(Client client, DataType dataType, boolean IsEncrypt, EncryptType entype, int priority, SignType signtype) {
        this(client, dataType, IsEncrypt, entype, priority, true, signtype);
    }

    public IProtocol(Client client, DataType dataType, boolean IsEncrypt, EncryptType entype, int priority, boolean needRec, SignType signtype) {
        this.client = client;
        this.dataType = dataType;
        this.IsEncrypt = IsEncrypt;
        this.entype = entype;
        this.priority = priority;
        this.needRec = needRec;
        this.signtype = signtype;
    }
    
    public abstract byte[] sender();
    
    public abstract void   receiver(byte[] data) throws InvalidPackageException;

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public boolean isIsEncrypt() {
        return IsEncrypt;
    }

    public void setIsEncrypt(boolean IsEncrypt) {
        this.IsEncrypt = IsEncrypt;
    }

    public EncryptType getEntype() {
        return entype;
    }

    public void setEntype(EncryptType entype) {
        this.entype = entype;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isNeedRec() {
        return needRec;
    }

    public void setNeedRec(boolean needRec) {
        this.needRec = needRec;
    }

    public SignType getSigntype() {
        return signtype;
    }

    public void setSigntype(SignType signtype) {
        this.signtype = signtype;
    }

    public int getErrorTimes() {
        return ErrorTimes;
    }

    public void setErrorTimes(int ErrorTimes) {
        this.ErrorTimes = ErrorTimes;
    }
    
    public void ConnectionTerminated(String message){
        Console.println(message);
        this.client.getEvents().clear();
        this.client.getEvents().add(new Close(client));
    }
    
    public void retry(){
        if(this.ErrorTimes < TryTimes){
            this.ErrorTimes++;
            this.client.getEvents().add(this);
        }else{
            ConnectionTerminated(String.format("Error: Tried more than %d times. Client will be closed.", TryTimes));
        }
    }
    
    public void reset(){
    }
    
    public void resetAndRetry(){
        reset();
        retry();
    }
}
