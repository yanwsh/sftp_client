/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Properties;
import sftp.lib.Console;
import sftp.lib.Utility;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.crypto.RSA;
import sftp.lib.socket.Entity.UserLogin;

/**
 * @file Config.java
 * @author Wensheng Yan
 * @date Mar 24, 2014
 */
public class Config {
    private static final String DEFAULT_FILENAME = "client.config.properties";
    private static final String DEFAULT_SEVURL = "127.0.0.1";
    private static final int DEFAULT_PORT = 8000;
    private static final String DEFAULT_USER = "";
    private static final int DEFAULT_RSALength = 1024;
    private static final String DEFAULT_SHAREDKEY = "iQx+6N5uVzIA0xhq73sNzH1jbPZRWMvoopgjEBrgedU=";
    private static final String DEFAULT_SERVPUBHASH = "8Jd1C0+tWTBcYe99pwTqVXHxCH3CW5MJBWRx65R/pwA=";
    private static final int DEFAULT_MaxPackageSize = 100000;
    private String ServerUrl;
    private Integer ServerPort;
    private ArrayList<UserLogin> savedUsers;
    private String autoUsername;
    private String lastUsername;
    private String key;
    //use for default authentication
    private String sharedKey;
    private String servPublicHash;
    private int maxPkgSize;

    public ArrayList<UserLogin> getSavedUsers() {
        if(savedUsers == null){
            savedUsers = new ArrayList<UserLogin>();
        }
        return savedUsers;
    }

    public void setSavedUsers(ArrayList<UserLogin> savedUsers) {
        this.savedUsers = savedUsers;
    }

    public String getLastUsername() {
        return lastUsername;
    }

    public void setLastUsername(String lastUsername) {
        this.lastUsername = lastUsername;
    }

    public String getAutoUsername() {
        return autoUsername;
    }

    public void setAutoUsername(String autoUsername) {
        this.autoUsername = autoUsername;
    }

    public int getMaxPkgSize() {
        return maxPkgSize;
    }

    public void setMaxPkgSize(int maxPkgSize) {
        this.maxPkgSize = maxPkgSize;
    }

    /**
     * Get the value of ServerPort
     *
     * @return the value of ServerPort
     */
    public Integer getServerPort() {
        return ServerPort;
    }

    /**
     * Set the value of ServerPort
     *
     * @param ServerPort new value of ServerPort
     */
    public void setServerPort(Integer ServerPort) {
        this.ServerPort = ServerPort;
    }

    /**
     * Get the value of ServerUrl
     *
     * @return the value of ServerUrl
     */
    public String getServerUrl() {
        return ServerUrl;
    }

    /**
     * Set the value of ServerUrl
     *
     * @param ServerUrl new value of ServerUrl
     */
    public void setServerUrl(String ServerUrl) {
        this.ServerUrl = ServerUrl;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSharedKey() {
        return sharedKey;
    }

    public void setSharedKey(String sharedKey) {
        this.sharedKey = sharedKey;
    }

    public String getServPublicHash() {
        return servPublicHash;
    }

    public void setServPublicHash(String servPublicHash) {
        this.servPublicHash = servPublicHash;
    }
    
    public static boolean SaveConfig(Config c){
	if(c == null){
            return false;
	}
	boolean flag = false;
	Properties prop = new Properties();
	OutputStream output = null;
	 
	try {
	 
            output = new FileOutputStream(DEFAULT_FILENAME);
	 
            // set the properties value
            prop.setProperty("sevurl", c.getServerUrl());
            prop.setProperty("port", Integer.toString(c.getServerPort()));
            prop.setProperty("savedUsers", Utility.printBase64Binary(ObjectHandler.ToBytes(c.getSavedUsers())));
            prop.setProperty("key", c.getKey());
            prop.setProperty("sharedkey", c.getSharedKey());
            prop.setProperty("servhash", c.getServPublicHash());
            prop.setProperty("autoUsername", c.getAutoUsername());
            prop.setProperty("lastUsername", c.getLastUsername());
            prop.setProperty("maxPkgSize", Long.toString(c.getMaxPkgSize()));
            // save properties to project root folder
            prop.store(output, null);
			
            flag = true;
	 
        } catch (IOException io) {
                   io.printStackTrace();
        } finally {
                if (output != null) {
                    try {
			output.close();
                    } catch (IOException e) {
			e.printStackTrace();
                    }
		}
	 
        }
	return flag;
    }
    
    public static String generateRSAKey(){
        RSA rsa = new RSA();
        KeyPair pair = rsa.generateKey(DEFAULT_RSALength);
	return Utility.printBase64Binary(ObjectHandler.ToBytes(pair));
    }
    
    

    public static Config LoadConfig(){
	Config c = new Config();
	Properties prop = new Properties();
	InputStream input = null;
	 
	try {
	 
            input = new FileInputStream(DEFAULT_FILENAME);
	 
            // load a properties file
            prop.load(input);
	 
            // get the property value and print it out
            c.setServerUrl(prop.getProperty("sevurl", DEFAULT_SEVURL));
            c.setServerPort(Utility.ParseInt(prop.getProperty("port"), DEFAULT_PORT));
            String str = prop.getProperty("savedUsers", "");
            if(!str.equals("")){
                Serializable s = ObjectHandler.FromBytes(Utility.parseBase64Binary(str));
                ArrayList<UserLogin> users = (ArrayList<UserLogin>)s;
                c.setSavedUsers(users);
            }else{
                c.setSavedUsers(new ArrayList<>());
            }
            c.setSharedKey(prop.getProperty("sharedkey", DEFAULT_SHAREDKEY));
            c.setServPublicHash(prop.getProperty("servhash", DEFAULT_SERVPUBHASH));
            c.setAutoUsername(prop.getProperty("autoUsername", DEFAULT_USER));
            c.setLastUsername(prop.getProperty("lastUsername", DEFAULT_USER));
            c.setMaxPkgSize(Utility.ParseInt(prop.getProperty("maxPkgSize"), DEFAULT_MaxPackageSize));
            c.setKey(prop.getProperty("key", ""));
	    if(c.getKey().equals("")){
	        c.setKey(generateRSAKey());
	        SaveConfig(c);
	    }
	} catch (IOException ex) {
            Console.println("Failed to load config file. Default value will be used.");
            c.setServerUrl(DEFAULT_SEVURL);
            c.setServerPort(DEFAULT_PORT);
            ArrayList<UserLogin> users = new ArrayList<>();
            c.setSavedUsers(users);
            c.setKey(generateRSAKey());
            c.setSharedKey(DEFAULT_SHAREDKEY);
            c.setServPublicHash(DEFAULT_SERVPUBHASH);
            c.setAutoUsername(DEFAULT_USER);
            c.setLastUsername(DEFAULT_USER);
            c.setMaxPkgSize(DEFAULT_MaxPackageSize);
            SaveConfig(c);
	} finally {
            if (input != null) {
		try {
                    input.close();
		} catch (IOException e) {
                    e.printStackTrace();
		}
            }
	}
	return c;
    }
    
    
}
