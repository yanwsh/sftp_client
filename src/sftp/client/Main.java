/*
 * Copyright (C) 2014 Wensheng Yan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package sftp.client;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import netscape.javascript.JSObject;
import sftp.client.protocol.Close;
import sftp.client.ui.BaseController;
import sftp.client.ui.LoginThread;
import sftp.lib.Version;
import sftp.lib.crypto.ObjectHandler;
import sftp.lib.socket.Entity.UserLogin;
import sftp_client.preloader.MessageProgressNotification;

/**
 * @file Main.java
 * @author Wensheng Yan
 * @date Mar 24, 2014
 */
public class Main extends Application {
    private final double MINIMUM_WINDOW_WIDTH = 100.0;
    private final double MINIMUM_WINDOW_HEIGHT = 100.0;
    public static boolean isApplet = false;
    private JSObject browser;
    private Stage stage;
    private Stage loginstage;
    private Client currentClient;
    public Config conf;
    private Image appicon;
    
    @Override
    public void init() throws Exception{               
        try {
            appicon = new Image(Utility.getFileFromResource(this,"/sftp/client/ui/images/app-icon.png"));
            notifyPreloader(new MessageProgressNotification(0.3, "Loading Config..."));
            conf = Config.LoadConfig();
            ObjectHandler.setSharedkey(conf.getSharedKey());
            Thread.sleep(500);
            notifyPreloader(new MessageProgressNotification(0.6, "Initialization..."));
            Thread.sleep(500);
            notifyPreloader(new MessageProgressNotification(0.7, "Connecting to Remote server..."));
            StartClient();
            notifyPreloader(new MessageProgressNotification(1,"finished."));
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeApplication(){
        if(currentClient != null && !currentClient.isInterrupted() && currentClient.isAlive()){
            currentClient.getEvents().add(new Close(currentClient));
            synchronized(currentClient){
                currentClient.notify();
            }
            try {
                currentClient.join(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Platform.exit();
        System.exit(0);
    }
    
    @Override
    public void start(Stage primaryStage) {
        // check if applet
        try {
            browser = getHostServices().getWebContext();
            isApplet =  browser != null;
        } catch (Exception e) {
            isApplet = false;
        }

        try {
            stage = primaryStage;
            stage.setTitle("SFTP " + Version.getVersion());
            stage.getIcons().add(appicon);
            stage.initStyle(StageStyle.DECORATED);
            stage.setMinWidth(MINIMUM_WINDOW_WIDTH);
            stage.setMinHeight(MINIMUM_WINDOW_HEIGHT);
            stage.centerOnScreen();
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    closeApplication();
                }
            });
            boolean needLoginPanel = true;
            LoginThread thread = null;
            if(!conf.getAutoUsername().equals("")){
                Object[] objs = conf.getSavedUsers().stream().filter((UserLogin user) -> {
                    return user.getUsername().equals(conf.getAutoUsername());
                }).toArray();
                if(objs.length > 0){
                    UserLogin defaultUser = (UserLogin)objs[0];
                    thread = new LoginThread(this, null, defaultUser.getUsername(), defaultUser.getPassword());
                    needLoginPanel = false;
                }
            }
            if(needLoginPanel)
                gotoLogin();
            else{
                if(thread != null){
                    thread.start();
                    thread.join();
                }
                if(currentClient.loginstate == ProtocolState.Success){
                    gotoMainFrameWithoutLogin();
                }else{
                    gotoLogin();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public void StartClient(){
        if(currentClient == null || !currentClient.isAlive()){
            currentClient = new Client(conf, Client.ClientType.Main); 
            currentClient.start();
        }
    }
    
    public void gotoLogin(){
        loginstage = new Stage();
                 
        //set title to stage
        loginstage.setTitle("Login");
        
        loginstage.getIcons().add(appicon);
        
        loginstage.initStyle(StageStyle.TRANSPARENT);
        
        //center stage on screen
        loginstage.centerOnScreen();
                 
        gotoFrame(loginstage, "Login.fxml", true, null);
        //show the stage
        loginstage.show();
        stage.hide();
    }
    
    public void gotoSetting(){
        Stage settingStage = new Stage();
        settingStage.setTitle("Setting");
        settingStage.getIcons().add(appicon);
        settingStage.centerOnScreen();
        gotoFrame(settingStage, "Setting.fxml", false, stage);
        settingStage.show();
    }
    
    public void gotoMainFrameWithoutLogin(){
        gotoFrame(stage, "MainFrame.fxml", false, null);
        stage.show(); 
    }
    
    public void gotoMainFrame(){
        Platform.runLater(()->{
            gotoFrame(stage, "MainFrame.fxml", false, null);
            stage.show(); 
            loginstage.hide();
        });
    }

    public Client getCurrentClient() {
        return currentClient;
    }

    public void setCurrentClient(Client currentClient) {
        this.currentClient = currentClient;
    }
    
    private void gotoFrame(Stage currentStage, String fxml, boolean windowbutton, Stage ParentStage) {
        try {
            BaseController controller =  (BaseController)BaseController.replaceSceneContent(currentStage, fxml, windowbutton, this);
            controller.setApp(this, currentStage);
            if(ParentStage != null){
                controller.setParentStage(ParentStage);
            }
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
